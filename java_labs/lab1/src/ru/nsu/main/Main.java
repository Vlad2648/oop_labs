package ru.nsu.main;

import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) {
        try {
            WordStats wordStats = new WordStats("input.txt");

            wordStats.parseFile();

            wordStats.saveFile("output.txt");
        } catch (FileNotFoundException e) {
            System.out.println("Error: Something wrong with file");
        }

    }

}
