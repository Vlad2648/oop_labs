package ru.nsu.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class WordStats {

    private File file;
    private Scanner scan;

    private Map<String, StoreClass> map;

    private int count;

    public WordStats(String fileName) throws FileNotFoundException {
        file = new File(fileName);
        scan = new Scanner(file).useDelimiter("[^\\p{L}0-9]+");
        map = new HashMap<>();
    }

    //Parse file and save data
    public void parseFile() {
        String word;

        while (scan.hasNext()) {
            word = scan.next();

            if (map.containsKey(word)) {
                map.put(word, new StoreClass(word, map.get(word).getCount() + 1));
            } else {
                map.put(word, new StoreClass(word));
            }

            count++;
        }

        scan.close();
    }

    //Save csv data into file with fileName
    public void saveFile(String fileName) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(fileName);

        List<StoreClass> list = new LinkedList<>(map.values());
        list.sort((a, b) -> Integer.compare(b.getCount(), a.getCount()));

        for (StoreClass c : list) {
            writer.println(c.getStr() + "," + c.getCount() + "," + String.format("%.2f", (c.getCount() / (double) count) * 100) + "%");
        }

        writer.close();
        map.clear();
    }
}
