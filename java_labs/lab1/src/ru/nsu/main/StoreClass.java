package ru.nsu.main;

import java.util.Objects;

public class StoreClass {

    private String str;

    private int count;

    public StoreClass(String str) {
        this.str = str;
        count = 1;
    }

    public StoreClass(String str, int count) {
        this.str = str;
        this.count = count;
    }

    public String getStr() {
        return str;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StoreClass that = (StoreClass) o;
        return count == that.count &&
                Objects.equals(str, that.str);
    }

    @Override
    public int hashCode() {

        return Objects.hash(str);
    }

}
