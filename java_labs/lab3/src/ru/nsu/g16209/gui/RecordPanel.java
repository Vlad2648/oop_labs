package ru.nsu.g16209.gui;

import ru.nsu.g16209.game.GameCore;
import ru.nsu.g16209.game.PlayerScope;

import javax.swing.*;
import java.awt.*;
import java.util.Comparator;
import java.util.List;

public class RecordPanel extends JDialog {

    private final DefaultListModel<String> listModel = new DefaultListModel<>();
    private final GameCore gameCore;

    RecordPanel(JFrame frame, GameCore gameCore) {
        super(frame, "Рекорды", true);
        this.gameCore = gameCore;
        JList<String> list = new JList<>(listModel);
        JScrollPane scrollPane = new JScrollPane(list);
        add(scrollPane);
        updateList();

        JButton closeButton = new JButton("Ок");
        closeButton.addActionListener(e -> setVisible(false));
        add(closeButton, BorderLayout.SOUTH);

        setSize(300, 200);
    }

    @Override
    public void setVisible(boolean b) {
        updateList();
        super.setVisible(b);
    }

    private void updateList() {
        listModel.clear();
        int pos = 1;
        List<PlayerScope> scopes = gameCore.getScores();
        scopes.sort(Comparator.comparingInt(PlayerScope::getTime));
        for (PlayerScope scope : scopes) {
            listModel.addElement(pos++ + ") " + scope.toString());
        }
    }
}
