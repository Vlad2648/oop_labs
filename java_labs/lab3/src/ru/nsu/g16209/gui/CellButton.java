package ru.nsu.g16209.gui;

import ru.nsu.g16209.game.GameCore;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

public class CellButton extends JButton {

    public static final int SIZE = 35;
    private static final Font TEXT_FONT = new Font("Arial", Font.BOLD, 16);

    private static Icon FLAG_ICON;
    private static Icon MINE_ICON;

    private FieldFrame fieldFrame;
    private final int posX;
    private final int posY;

    CellButton(FieldFrame frame, int x, int y) {
        if ((FLAG_ICON == null) || (MINE_ICON == null)) {
            throw new RuntimeException("Couldn't load images");
        }

        fieldFrame = frame;
        posX = x;
        posY = y;
        addMouseListener(new GameListener());
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setBackground(Color.GRAY);
    }

    public void updateState(int state) {
        setIcon(null);
        setBackground(Color.WHITE);
        setText("");
        switch (state) {
            case GameCore.FLAG:
                setIcon(FLAG_ICON);
                setBackground(Color.GRAY);
                break;

            case GameCore.MINE:
                setIcon(MINE_ICON);
                setBackground(Color.RED);
                break;

            case GameCore.CLOSE:
                setIcon(null);
                setBackground(Color.GRAY);
                break;
            case 0:
                setIcon(null);
                setBackground(Color.WHITE);
                break;

            default:
                setIcon(null);
                setText(String.valueOf(state));
                setFont(TEXT_FONT);
                setForeground(Color.BLACK);
                break;

        }
    }

    private class GameListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {

        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            if ((mouseEvent.getX() < getWidth()) && (mouseEvent.getY() <= getHeight())) {
                    clickButton(mouseEvent);
            }
        }

        private void clickButton(MouseEvent mouseEvent) {
            if ((mouseEvent.getButton() == MouseEvent.BUTTON1) && (getIcon() != FLAG_ICON)) {
                fieldFrame.openCell(posX, posY);
            } else if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
                fieldFrame.setFlag(posX, posY);
            }
        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {

        }
    }

    static {
        try {
            FLAG_ICON = new ImageIcon(ImageIO.read(CellButton.class.getResource("resources/flag.png"))
                    .getScaledInstance(SIZE, SIZE, Image.SCALE_DEFAULT));
            MINE_ICON = new ImageIcon(ImageIO.read(CellButton.class.getResource("resources/mine.png"))
                    .getScaledInstance(SIZE, SIZE, Image.SCALE_DEFAULT));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
