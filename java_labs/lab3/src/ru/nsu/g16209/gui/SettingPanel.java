package ru.nsu.g16209.gui;

import ru.nsu.g16209.game.GameCore;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.text.NumberFormat;

public class SettingPanel extends JDialog {

    private static final String DEFAULT_NAME = "noname";

    private final JTextField name;
    private final JFormattedTextField mines;
    private final JFormattedTextField height;
    private final JFormattedTextField width;
    private final MainFrame mainFrame;

    SettingPanel(MainFrame frame) {
        super(frame, "Настройки", true);
        mainFrame = frame;

        name = new JTextField(DEFAULT_NAME);

        NumberFormat format = NumberFormat.getInstance();
        NumberFormatter minesFormatter = new NumberFormatter(format);
        minesFormatter.setValueClass(Integer.class);

        minesFormatter.setMaximum(2500);
        minesFormatter.setMinimum(0);
        mines = new JFormattedTextField(minesFormatter);
        mines.setValue(GameCore.DEFAULT_MINES);

        NumberFormatter sizesFormatter = new NumberFormatter(format);
        sizesFormatter.setValueClass(Integer.class);
        sizesFormatter.setMaximum(50);
        sizesFormatter.setMinimum(5);

        height = new JFormattedTextField(sizesFormatter);
        height.setValue(GameCore.DEFAULT_HEIGTH);
        width = new JFormattedTextField(sizesFormatter);
        width.setValue(GameCore.DEFAULT_WIDTH);

        JButton closeButton = new JButton("ok");
        closeButton.addActionListener(e -> closeSetting());

        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        add(new JLabel("Имя:"));
        add(name);
        add(new JLabel("Ширина (от 5 до 50):"));
        add(width);
        add(new JLabel("Высота (от 5 до 50):"));
        add(height);
        add(new JLabel("Кол-во мин (от 0 до 2500):"));
        add(mines);
        add(new JLabel("Закрыть"));
        add(closeButton);

        setSize(330, 250);
        setResizable(false);
    }

    public int getSettingHeight() {
        return height.getValue() == null ? -1 : (int) height.getValue();
    }

    public int getSettingWidth() {
        return width.getValue() == null ? -1 : (int) width.getValue();
    }

    public int getSettingMines() {
        return mines.getValue() == null ? -1 : (int) mines.getValue();
    }

    public String getSettingName() {
        return name.getText();
    }

    private void closeSetting() {
        int mines = getSettingMines();
        int height = getSettingHeight();
        int width = getSettingWidth();
        if ((mines == -1) || (height == -1) || (width == -1) || getSettingName().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Проверте введеные данные", "Ошибка",
                    JOptionPane.WARNING_MESSAGE);
        }
        if (mines >= width * height) {
            JOptionPane.showMessageDialog(this, "Нельзя разместить столько мин", "Ошибка",
                    JOptionPane.WARNING_MESSAGE);
        }
        setVisible(false);
        mainFrame.restartGame();
    }
}
