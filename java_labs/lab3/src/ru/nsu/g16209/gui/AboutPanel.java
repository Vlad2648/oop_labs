package ru.nsu.g16209.gui;

import javax.swing.*;
import java.awt.*;

public class AboutPanel extends JDialog{

    private final JLabel about = new JLabel("Эта игра создана Башевым Владиславом");

    AboutPanel(MainFrame frame) {
        super(frame, "О игре", true);

        add(about, BorderLayout.CENTER);

        JButton closeButton = new JButton("Ок");

        closeButton.addActionListener(e -> setVisible(false));

        add(closeButton, BorderLayout.SOUTH);
        setSize(300, 150);
    }
}
