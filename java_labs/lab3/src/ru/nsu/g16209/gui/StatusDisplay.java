package ru.nsu.g16209.gui;

import ru.nsu.g16209.game.GameTimer;

import javax.swing.*;
import java.awt.*;

public class StatusDisplay extends Label implements Runnable {

    private static final Font FONT = new Font("Arial", Font.BOLD, 12);

    private final GUITimer timer;

    private String name;
    private volatile int currentTime;

    StatusDisplay(String name) {
        this.name = name;
        timer = new GUITimer();
        setFont(FONT);
        updateDisplay();
    }

    public void resetGame() {
        timer.resetTimer();
        updateDisplay();
    }

    public void startTimer() {
        timer.startTimer();
    }

    public void stopTimer() {
        timer.stopTimer();
    }

    public void setName(String name) {
        this.name = name;
        updateDisplay();
    }

    @Override
    public void run() {
        updateDisplay();
    }

    private void updateDisplay() {
        int time;
        synchronized (timer) {
            time = currentTime;
        }
        String text = formString(time);
        setText(text);

        FontMetrics metrics = getFontMetrics(getFont());
        setSize(metrics.stringWidth(text), metrics.getHeight());
    }

    public int getMaxWidth() {
        return getFontMetrics(getFont()).stringWidth(formString(1000));
    }

    private String formString(int time) {
        return String.format("Time: %d, Name: \"%s\" ", time, name);
    }

    private class GUITimer extends GameTimer{

        @Override
        protected void updateTimer(int curTime) {
            super.updateTimer(curTime);
            synchronized (this) {
                currentTime = curTime;
            }
            SwingUtilities.invokeLater(StatusDisplay.this);
        }
    }
}
