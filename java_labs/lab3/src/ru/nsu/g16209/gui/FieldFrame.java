package ru.nsu.g16209.gui;

import ru.nsu.g16209.game.GameCore;

import javax.swing.*;
import java.awt.*;

public class FieldFrame extends JPanel {

    private static final Font STATE_FONT = new Font("Arial", Font.BOLD, 40);

    private final MainFrame frame;
    private final StatusDisplay statusDisplay;
    private final GameCore gameCore;
    private CellButton[][] buttons;

    private boolean isStart = false;

    private int height;
    private int width;

    FieldFrame(MainFrame frame, StatusDisplay display, GameCore gameCore) {
        this.frame = frame;
        this.statusDisplay = display;
        this.gameCore = gameCore;
        height = gameCore.getHeight();
        width = gameCore.getWidth();

        initField();
    }

    public void restartGame(int width, int height, int mines) {
        this.width = width;
        this.height = height;
        gameCore.createNewField(width, height, mines);
        initField();
    }

    public void openCell(int posX, int posY) {
        if (!isStart) {
            startOnClick();
        }
        int result = gameCore.openCell(posX, posY);
        updateField();
        if (result == GameCore.NONE) {
            return;
        }
        isStart = false;
        gameCore.stopGame();
        gameCore.saveScore(frame.getName());
        statusDisplay.stopTimer();
        int returnCode = 0;
        if (result == GameCore.WIN) {
            returnCode = JOptionPane.showConfirmDialog(frame, "Вы выиграли! Перезапустить игру?", "Сообщение", JOptionPane.YES_NO_OPTION);
        } else if (result == GameCore.LOSE) {
            returnCode = JOptionPane.showConfirmDialog(frame, "Вы проиграли! Перезапустить игру?", "Сообщение", JOptionPane.YES_NO_OPTION);
        }
        if (returnCode == JOptionPane.YES_OPTION) {
            restartGame(width, height, gameCore.getCountMines());
        }
    }

    public void setFlag(int posX, int posY) {
        if (!isStart) {
            startOnClick();
        }
        gameCore.setFlag(posX, posY);
        updateField();
    }

    private void startOnClick() {
        isStart = true;
        gameCore.startGame();
        statusDisplay.startTimer();
    }

    private void initField() {
        removeAll();

        setSize(height * CellButton.SIZE, width * CellButton.SIZE);
        setLayout(new GridLayout(width, height));

        buttons = new CellButton[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                add(buttons[i][j] = new CellButton(this, j, i));
            }
        }
        validate();
        repaint();
    }

    private void updateField() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                buttons[i][j].updateState(gameCore.cellState(j, i));
                buttons[i][j].repaint();
            }
        }
        repaint();
    }
}
