package ru.nsu.g16209.gui;

import ru.nsu.g16209.game.GameCore;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MainFrame extends JFrame {

    private final GameCore gameCore;

    private String name = "noname";

    private final FieldFrame fieldFrame;
    private final StatusDisplay statusDisplay;
    private final AboutPanel aboutPanel;
    private final RecordPanel recordPanel;
    private final SettingPanel settingPanel;

    public MainFrame() {
        super("Сапер");

        //Инициализация полей
        gameCore = new GameCore();
        statusDisplay = new StatusDisplay(name);

        fieldFrame = new FieldFrame(this, statusDisplay, gameCore);
        aboutPanel = new AboutPanel(this);
        recordPanel = new RecordPanel(this, gameCore);
        settingPanel = new SettingPanel(this);

        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints cn = new GridBagConstraints();
        setLayout(layout);

        resizeWindow();

        //Создание поля
        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.add(fieldFrame);
        cn.gridx = 0;
        cn.gridy = 0;
        cn.gridwidth = gameCore.getWidth();
        cn.gridheight = gameCore.getHeight();
        cn.weightx = 1;
        cn.weighty = 1;
        cn.anchor = GridBagConstraints.CENTER;
        cn.fill = GridBagConstraints.BOTH;
        cn.insets = new Insets(5, 5, 0, 0);
        layout.setConstraints(panel, cn);
        add(panel);

        //Создание статуса и кнопок
        cn.gridx = gameCore.getWidth();
        cn.gridwidth = 5;
        cn.gridheight = 1;
        cn.weightx = 0;
        cn.weighty = 0;
        cn.insets = new Insets(0, 10, 0, 10);

        cn.gridy = 0;
        layout.setConstraints(statusDisplay, cn);
        add(statusDisplay);

        //Создание кнопки перезапуска
        JButton restartButton = new JButton("Перезапуск");
        restartButton.addActionListener(e -> restartGame());
        cn.gridy = 1;
        layout.setConstraints(restartButton, cn);
        add(restartButton);

        //Создание кнопки настройки
        JButton settingButton = new JButton("Настройки");
        settingButton.addActionListener(e -> pressSetting());
        cn.gridy = 2;
        layout.setConstraints(settingButton, cn);
        add(settingButton);

        //Создание кнопки о игре
        JButton aboutButton = new JButton("О игре");
        aboutButton.addActionListener(e -> pressAbout());
        cn.gridy = 3;
        layout.setConstraints(aboutButton, cn);
        add(aboutButton);

        //Создание кнопки рекорды
        JButton recordButton = new JButton("Рекорды");
        recordButton.addActionListener(e -> pressRecord());
        cn.gridy = 4;
        layout.setConstraints(recordButton, cn);
        add(recordButton);

        //Создание кнопки выход
        JButton exitButton = new JButton("Выход");
        exitButton.addActionListener(e -> dispose());
        cn.gridy = 5;
        layout.setConstraints(exitButton, cn);
        add(exitButton);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(true);
        setVisible(true);

    }

    private void resizeWindow() {
        setSize(fieldFrame.getWidth()  + statusDisplay.getMaxWidth() + 35, fieldFrame.getHeight() + 45);
        setMinimumSize(new Dimension(getWidth(), getHeight()));
    }

    public void restartGame() {
        name = settingPanel.getSettingName();
        fieldFrame.restartGame(settingPanel.getSettingWidth(), settingPanel.getSettingHeight(), settingPanel.getSettingMines());
        statusDisplay.setName(name);
        resizeWindow();
    }

    private void pressAbout() {
        aboutPanel.setVisible(true);
    }

    private void pressRecord() {
        recordPanel.setVisible(true);
    }

    private void pressSetting() {
        settingPanel.setVisible(true);
    }

    public String getName() {
        return name;
    }

    private class MainGameListener implements MouseListener {

        private final PressListener listener;

        MainGameListener(PressListener listener) {
            this.listener = listener;
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {

        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            if (mouseEvent.getButton() == MouseEvent.BUTTON1) {
                listener.press();
            }
        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {

        }
    }

    private interface PressListener {
        void press();
    }
}
