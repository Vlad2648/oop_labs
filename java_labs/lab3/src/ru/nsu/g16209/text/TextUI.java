package ru.nsu.g16209.text;

import ru.nsu.g16209.game.GameCore;
import ru.nsu.g16209.game.PlayerScope;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class TextUI {

    private static final char MINE = '*';
    private static final char CLOSE = '#';
    private static final char FLAG = 'f';

    private BufferedReader reader;
    private GameCore gameCore;

    private int width;
    private int height;

    public TextUI() {
        reader = new BufferedReader(new InputStreamReader(System.in));
        gameCore = new GameCore();

        width = gameCore.getWidth();
        height = gameCore.getHeight();
    }

    public void startMineSweeper() {
        String line;

        while (true) {
            System.out.println("Menu (chose needed number):");
            System.out.println("1. New Game");
            System.out.println("2. High Scores");
            System.out.println("3. About");
            System.out.println("4. Exit");
            System.out.println();

            int command = 0;
            try{
                line = reader.readLine();
                command = Integer.parseInt(line);
            } catch (IOException e) {
                System.out.println();
                System.out.println("Wrong input");
                System.out.println();
                continue;
            }

            if ((command <= 0) || (command >= 5)) {
                System.out.println("Chose in range [1, 4]");
                continue;
            }

            switch (command) {
                case 1:
                    startGame();
                    break;
                case 2:
                    highScopes();
                    break;
                case 3:
                    about();
                    break;
                case 4:
                    return;
            }
        }


    }

    private void startGame() {
        int posX;
        int posY;
        String line;
        String[] command;
        int returnValue = 0;

        gameCore.createNewField(gameCore.getWidth(), gameCore.getHeight(), gameCore.getCountMines());
        gameCore.startGame();

        while (true) {
            renderField();
            try{
                line = reader.readLine();
            } catch (IOException e) {
                System.out.println();
                System.out.println("Wrong input");
                System.out.println();
                continue;
            }
            command = line.split("[ ]+");
            if (command.length != 2 && command.length != 3) {
                System.out.println();
                System.out.println("Wrong input");
                System.out.println();
                continue;
            }
            try {
                if (command[0].equals("f")) {
                    posX = Integer.parseInt(command[1]) - 1;
                    posY = Integer.parseInt(command[2]) - 1;
                    gameCore.setFlag(posX, posY);
                    continue;
                } else {
                    posX = Integer.parseInt(command[0]) - 1;
                    posY = Integer.parseInt(command[1]) - 1;
                }
            } catch (NumberFormatException e) {
                System.out.println();
                System.out.println("Wrong input");
                System.out.println();
                continue;
            }

            try {
                returnValue = gameCore.openCell(posX, posY);
            } catch (IllegalArgumentException e) {
                System.out.println(e);
                break;
            }
            if (returnValue != GameCore.NONE) {
                break;
            }
        }

        renderField();
        if (returnValue == GameCore.WIN) {
            saveGame();
        }
        if (returnValue == GameCore.LOSE) {
            System.out.println("You lose");
        }
        System.out.println();
    }

    private void saveGame() {
        System.out.println();
        System.out.println("You won!!!!");
        System.out.println("Enter your name: ");
        String name = null;
        try {
            name = reader.readLine();
        } catch (IOException e) {
            name = "user";
        }

        gameCore.saveScore(name);
    }

    private void about() {
        System.out.println();
        System.out.println("This game created Bashev Vladislav");
        System.out.println();
    }

    private void highScopes() {
        List<PlayerScope> scores = gameCore.getScores();

        if (scores.isEmpty()) {
            System.out.println("Nothing");
            return;
        }

        scores.sort(Comparator.comparingInt(PlayerScope::getTime));

        System.out.println();
        System.out.println("Scopes:");
        for (PlayerScope playerScope : scores) {
            System.out.println(playerScope.toString());
        }
        System.out.println();
    }

    private void renderField() {

        int returnValue;
        char symbol;

        System.out.print("№   ");
        for (int i = 1; i <= width; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println();

        for (int i = 0; i < height; i++) {
            System.out.print((i + 1) + "   ");
            for (int j = 0; j < width; j++) {
                try{
                    returnValue = gameCore.cellState(j, i);
                } catch (IllegalArgumentException e) {
                    System.out.println(i + " " + j + " " + e);
                    return;
                }

                if (returnValue == GameCore.CLOSE) {
                    symbol = CLOSE;
                } else if (returnValue == GameCore.MINE) {
                    symbol = MINE;
                } else if (returnValue == GameCore.FLAG) {
                    symbol = FLAG;
                } else {
                        symbol = '0';
                        symbol += (char)returnValue;
                }

                System.out.print(symbol + " ");
            }
            System.out.println();
        }
    }

}
