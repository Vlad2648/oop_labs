package ru.nsu.g16209.main;

import ru.nsu.g16209.gui.MainFrame;
import ru.nsu.g16209.text.TextUI;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Start GUI?");
        String answer = scanner.nextLine();
        if (answer.equals("yes") || answer.equals("y") || answer.equals("")) {
            MainFrame  mainFrame = new MainFrame();
        } else {
            TextUI textUI = new TextUI();
            textUI.startMineSweeper();
        }
    }
}
