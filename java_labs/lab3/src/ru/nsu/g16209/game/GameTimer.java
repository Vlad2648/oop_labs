package ru.nsu.g16209.game;

import java.util.Timer;
import java.util.TimerTask;

public class GameTimer {

    private Timer timer = new Timer(true);
    private boolean run;

    private long begin;
    private long currentTime;

    private TimerTask hiddenTimer;

    /**
     * Запускает таймер
     */
    public void startTimer() {
        stopTimer();
        begin = System.currentTimeMillis();
        run = true;
        hiddenTimer = new HiddenTimer();
        timer.schedule(hiddenTimer, 0, 1000);
    }

    /**
     * Останавливает таймер
     */
    public void stopTimer() {
        if (hiddenTimer != null) {
            hiddenTimer.cancel();
        }
        run = false;
    }

    /**
     * @return Возвращает время если таймер остановлен, иначе 0
     */
    public int getTime() {
        return (int)currentTime;
    }

    /**
     * Перезапускает таймер
     */
    public void resetTimer() {
        stopTimer();
        startTimer();
    }

    /**
     * @return Состояние таймера
     */
    public boolean isRun() {
        return run;
    }

    private int getCurrentTime() {
        return (int) (System.currentTimeMillis() - begin) / 1000;
    }

    protected void updateTimer(int curTime) {
        currentTime = curTime;
    }

    private class HiddenTimer extends TimerTask {

        @Override
        public void run() {
            updateTimer(getCurrentTime());
        }
    }
}
