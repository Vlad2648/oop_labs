package ru.nsu.g16209.game;

public class PlayerScope {

    private final String name;
    private final int square;
    private final int time;

    /**
     * @param name Имя игрока
     * @param square Размер поля
     * @param time Время игры
     */
    PlayerScope(String name, int square, int time) {
        this.name = name;
        this.square = square;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public int getSquare() {
        return square;
    }

    public int getTime() {
        return time;
    }

    @Override
    public String toString() {
        return String.format("Name: %s, square: %d, time: %d", name, square, time);
    }
}
