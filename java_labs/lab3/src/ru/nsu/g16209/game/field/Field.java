package ru.nsu.g16209.game.field;

/**
 * Игоровое поле
 */
public abstract class Field {

    public static final int CLOSE = -1;
    public static final int FLAG = -2;
    public static final int MINE = -3;


    protected final int width;
    protected final int height;
    protected final int mines;

    protected final Cell[][] field;

    /**
     * @param width Ширина поля
     * @param height Высота поля
     * @param mines Количество мин
     *
     * @throws IllegalArgumentException Если входные значения меньше или равны 0
     */
    Field(int width, int height, int mines) {

        if ((width <= 0) || (height <= 0) || (mines <= 0)) {
            throw new IllegalArgumentException("width and height must be greater then zero");
        }
        this.width = width;
        this.height = height;
        this.mines = mines;

        field = new Cell[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                field[i][j] = new Cell();
            }
        }

        initializeField();
    }

    /**
     * Открывает клетку
     * @param posX Позиция по координате X
     * @param posY Позиция по координате Y
     *
     * @throws IllegalArgumentException Если входные значения не входят в диапозон [0; width/height)
     * @return MINE Если открыта мина или количество открытых клеток в противном случае
     */
    public int openCell(int posX, int posY) {
        if ((posX < 0) || (posX >= width)) {
            throw new IllegalArgumentException("posX must be in range [0; width)");
        }
        if ((posY < 0) || (posY >= height)) {
            throw new IllegalArgumentException("posY must be in range [0; height)");
        }

        if (field[posY][posX].isFlag() || field[posY][posX].isOpen()) {
            return 0;
        }

        int openCells = 1;
        field[posY][posX].open();
        if (field[posY][posX].isMine()) {
            return MINE;
        }

        if (field[posY][posX].getCountMineNeighbors() == 0) {
            long group = field[posY][posX].getGroup();
            for (int i = 0; i < height; i++) {
                for( int j = 0; j < width; j++) {
                    if ((field[i][j].getGroup() & group) != 0) {
                        field[i][j].open();
                        openCells++;
                    }
                }
            }
        }
        return openCells;
    }

    /**
     * @param posX Позиция по координате X
     * @param posY Позиция по координате Y
     *
     * @throws IllegalArgumentException Если входные значения не входят в диапозон [0; width/height)
     * @return CLOSE если клетка закрыта, FLAG если установлен флаг, MINE если это мина или число соседних мин в противном случае
     */
    public int getXYState(int posX, int posY) {
        if ((posX < 0) || (posX >= width)) {
            throw new IllegalArgumentException("posX must be in range [0; width)");
        }
        if ((posY < 0) || (posY >= height)) {
            throw new IllegalArgumentException("posY must be in range [0; height)");
        }

        if (field[posY][posX].isFlag()) {
            return FLAG;
        }
        if (!field[posY][posX].isOpen()) {
            return CLOSE;
        }
        if (field[posY][posX].isMine()) {
            return MINE;
        }
        return field[posY][posX].getCountMineNeighbors();
    }

    /**
     * Ставит флаг
     * @param posX Координата на оси X
     * @param posY Координата на оси Y
     */
    public void setFlag(int posX, int posY) {
        field[posY][posX].setFlag();
    }

    /**
     * Убирает флаг
     * @param posX Координата на оси X
     * @param posY Координата на оси Y
     */
    public void unsetFlag(int posX, int posY) {
        field[posY][posX].unsetFlag();
    }

    /**
     * Инициализирует поле минами
     */
    public abstract void initializeField();

}
