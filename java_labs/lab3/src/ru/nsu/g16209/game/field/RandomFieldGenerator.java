package ru.nsu.g16209.game.field;

import java.util.Random;

public class RandomFieldGenerator extends Field {

    public RandomFieldGenerator(int width, int height, int mines) {
        super(width, height, mines);
    }

    /**
     * Инициализирует поле минами или пересоздает поле
     */
    public void initializeField() {
        setMines();
        countNeighbors();
        setGroups();
    }

    /**
     * Устанавливает мины в произвольном порядке
     */
    private void setMines() {
        int[] rand = new int[width * height];
        for (int i = 0; i < width * height; i++) {
            rand[i] = i;
        }

        Random random = new Random();
        int pos;
        for (int i = 0; i < mines; i++) {
            pos = random.nextInt(width * height);
            rand[i] = rand[i] ^ rand[pos];
            rand[pos] = rand[i] ^ rand[pos];
            rand[i] = rand[i] ^ rand[pos];
        }

        for (int i = 0; i < mines; i++) {
            field[rand[i] / width][rand[i] % width].setMine();
        }
    }

    /**
     * Подсчитывает соседей для мин
     */
    private void countNeighbors() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (field[i][j].isMine()) {
                    addNeighborMinesCount(j, i);
                }
            }
        }
    }

    /**
     * Считает количество соседей отдельной клетки
     */
    @SuppressWarnings("Duplicates")
    private void addNeighborMinesCount(int posX, int posY) {
        int dx;
        int dy;
        for (int i = 0; i < 8; i++) {
            dx = posX;
            dy = posY;
            switch (i) {
                case 0: case 3: case 5:
                    dx--;
                    break;
                case 2: case 4: case 7:
                    dx++;
            }
            switch (i) {
                case 0: case 1: case 2:
                    dy--;
                    break;
                case 5: case 6: case 7:
                    dy++;
            }
            if ((dx < 0) || (dx >= width) || (dy < 0) || (dy >= height)) {
                continue;
            }

            field[dy][dx].addCountMineNeighbors();
        }
    }

    /**
     * Считает группы для открытия
     */
    private void setGroups() {
        long group = 1;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if ((!field[i][j].isMine()) && (field[i][j].getCountMineNeighbors() == 0)) {
                    setSector(j, i, group);
                    group <<= 1;
                }
            }
        }
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if ((!field[i][j].isMine()) && (field[i][j].getCountMineNeighbors() != 0)) {
                    group = findGroup(j, i);
                    field[i][j].setGroup(group);
                }
            }
        }
    }

    /**
     * Рекурсивно нахоит сектор сосотоящий из 0 и устанавливает на нем группу
     * @param posX Координата на оси X
     * @param posY Координата на оси Y
     * @param group Группа
     */
    private void setSector(int posX, int posY, long group) {
        if ((posX < 0) || (posX >= width) || (posY < 0) || (posY >= height)) {
            return;
        }
        if ((field[posY][posX].getGroup() != 0) || (field[posY][posX].isMine()) || (field[posY][posX].getCountMineNeighbors() != 0)) {
            return;
        }
        field[posY][posX].setGroup(group);
        setSector(posX + 1, posY, group);
        setSector(posX, posY + 1, group);
        setSector(posX - 1, posY, group);
        setSector(posX, posY - 1, group);
    }

    /**
     * Находит все группы соседних клеток с 0
     * @param posX Координата на оси X
     * @param posY Координата на оси Y
     * @return Группа соседей
     */
    @SuppressWarnings("Duplicates")
    private long findGroup(int posX, int posY) {
        int dx;
        int dy;
        long result = 0;
        for (int i = 0; i < 8; i++) {
            dx = posX;
            dy = posY;
            switch (i) {
                case 0: case 3: case 5:
                    dx--;
                    break;
                case 2: case 4: case 7:
                    dx++;
            }
            switch (i) {
                case 0: case 1: case 2:
                    dy--;
                    break;
                case 5: case 6: case 7:
                    dy++;
            }
            if ((dx < 0) || (dx >= width) || (dy < 0) || (dy >= height)) {
                continue;
            }

            if (!field[dy][dx].isMine() && (field[dy][dx].getCountMineNeighbors() == 0)) {
                result |= field[dy][dx].getGroup();
            }
        }
        return result;
    }
}
