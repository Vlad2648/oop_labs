package ru.nsu.g16209.game.field;

/**
 * Игровая клетка
 */
public class Cell {

    private boolean isOpen = false;
    private long group = 0;
    private boolean mine = false;
    private int countMineNeighbors = 0;
    private boolean flag = false;

    /**
     * Создает клетку без мины с 0 соседей и 0 группой
     */
    Cell() {
    }

    /**
     * Создает клутку с 0 соседей и 0 группой
     * @param mine Состояние мины
     */
    Cell(boolean mine) {
        this.mine = mine;
    }

    /**
     * Открывает клетку
     */
    public void open() {
        isOpen = true;
    }

    /**
     * Ставит флаг
     */
    void setFlag() {
        flag = true;
    }

    /**
     * Убирает флаг
     */
    void unsetFlag() {
        flag = false;
    }

    /**
     * @return Состояние флага
     */
    public boolean isFlag() {
        return flag;
    }

    /**
     * @return Возвращает состояние клетки (открыта или нет)
     */
    public boolean isOpen() {
        return isOpen;
    }

    /**
     * Задает номер группы
     * @param group Номер группы
     */
    public void setGroup(long group) {
        this.group = group;
    }

    /**
     * @return Возвращает номер группы
     */
    public long getGroup() {
        return group;
    }

    /**
     * Задает состояние мины в клетке
     */
    public void setMine() {
        this.mine = true;
    }

    /**
     * @return Возвращает состояние мины
     */
    public boolean isMine() {
        return mine;
    }

    /**
     * Увеличивает количество соседних мин на 1 если это не мина
     */
    public void addCountMineNeighbors() {
        if (!mine) {
            countMineNeighbors++;
        }
    }

    /**
     * Устанавливает количество мин
     * @param countMineNeighbors количество мин
     */
    public void setCountMineNeighbors(int countMineNeighbors) {
        this.countMineNeighbors = countMineNeighbors;
    }

    /**
     * @return Количество соседних мин
     */
    public int getCountMineNeighbors() {
        if (mine) {
            return 0;
        } else {
            return countMineNeighbors;
        }
    }
}
