package ru.nsu.g16209.game;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import ru.nsu.g16209.game.field.Field;
import ru.nsu.g16209.game.field.RandomFieldGenerator;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Ядро игры
 */
public class GameCore {

    public static final int CLOSE = Field.CLOSE;
    public static final int FLAG = Field.FLAG;
    public static final int MINE = Field.MINE;

    public static final int WIN = 11;
    public static final int LOSE = 12;
    public static final int NONE = 10;

    public static final int DEFAULT_WIDTH = 9;
    public static final int DEFAULT_HEIGTH = 9;
    public static final int DEFAULT_MINES = 10;

    private int width;
    private int height;
    private int mines;
    private int correctCell;

    private Gson GSON;
    private static final String saveFileName = "save.json";

    private List<PlayerScope> scores;

    private Field field;
    private GameTimer gameTimer;

    /**
     * Создает стандарную игру с полем 9х9 и с 10 минами
     */
    public GameCore() {
        this(DEFAULT_WIDTH, DEFAULT_HEIGTH, DEFAULT_MINES);
    }

    /**
     * Создает игоровое поле
     * @param width Ширина поля
     * @param height Высота поля
     * @param mines Количество мин
     *
     * @throws IllegalArgumentException Если параметры меньше или равны чем 0
     */
    public GameCore(int width, int height, int mines) {
        initField(width, height, mines);
        initGSON();
        loadScore();
        gameTimer = new GameTimer();
    }

    /**
     * Начинает вести счет времени
     */
    public void startGame() {
        gameTimer.startTimer();
    }

    /**
     * Заканчивает отсчет времени
     */
    public void stopGame() {
        gameTimer.stopTimer();
    }

    /**
     * Возвращает состояние клетки
     * @param posX Позиция по координате X
     * @param posY Позиция по координате Y
     *
     * @throws IllegalArgumentException Если входные значения не входят в диапозон [0; width/height)
     * @return CLOSE если клетка закрыта, FLAG если установлен флаг, MINE если это мина или число соседних мин в противном случае
     */
    public int cellState(int posX, int posY) {
        return field.getXYState(posX, posY);
    }

    /**
     * Открывает клетку
     * @param posX Позиция по координате X
     * @param posY Позиция по координате Y
     *
     * @throws IllegalArgumentException Если входные значения не входят в диапозон [0; width/height)
     * @return LOSE Если мина взорвалась, WIN если игра выиграна, NONE если ничего
     */
    public int openCell(int posX, int posY) {
        int returnValue = field.openCell(posX, posY);
        if (returnValue == MINE) {
            stopGame();
            return LOSE;
        }
        correctCell -= returnValue;
        if (correctCell <= 0) {
            stopGame();
            return WIN;
        }
        return NONE;
    }

    /**
     * Изменяет состояние флага
     * @param posX Координата на оси X
     * @param posY Координата на оси Y
     */
    public void setFlag(int posX, int posY) {
        if (field.getXYState(posX, posY) == Field.FLAG) {
            field.unsetFlag(posX, posY);
        } else {
            if (field.getXYState(posX, posY) == Field.CLOSE) {
                field.setFlag(posX, posY);
            }
        }
    }

    /**
     * Сохраняет результат игры
     * @param name Имя игрока
     */
    public void saveScore(String name) {
        scores.add(new PlayerScope(name, width * height, gameTimer.getTime()));
        try {
            File file = new File(saveFileName);
            Writer writer = new FileWriter(file);
            GSON.toJson(scores, new TypeToken<LinkedList<PlayerScope>>(){}.getType(), writer);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * Загружает файл с результатами игры
     */
    private void loadScore() {
        try {
            File file = new File(saveFileName);
            Reader reader = new FileReader(file);
            scores = GSON.fromJson(reader, new TypeToken<LinkedList<PlayerScope>>(){}.getType());
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (scores == null) {
            scores = new LinkedList<>();
        }
    }

    /**
     * @return Список сохраненных игр
     */
    public List<PlayerScope> getScores() {
        return scores;
    }

    public void createNewField(int width, int height, int mines) {
        initField(width, height, mines);
    }

    private void initField(int width, int height, int mines) {
        this.width = width;
        this.height = height;
        this.mines = mines;
        correctCell = width * height - mines;

        field = new RandomFieldGenerator(width, height, mines);
        //field.initializeField();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getCountMines() {
        return mines;
    }

    private void initGSON() {
        GsonBuilder builder = new GsonBuilder();
        JsonDeserializer<PlayerScope> deserializer = (json, type, context) -> {
            JsonObject jsonObject = json.getAsJsonObject();
            String name = jsonObject.get("name").getAsString();
            int square = jsonObject.get("square").getAsInt();
            int time = jsonObject.get("time").getAsInt();
            return new PlayerScope(name, square, time);
        };
        builder.registerTypeAdapter(PlayerScope.class, deserializer);
        builder.setPrettyPrinting();
        GSON = builder.create();
    }
}
