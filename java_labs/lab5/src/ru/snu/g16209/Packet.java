package ru.snu.g16209;

import java.io.Serializable;

public class Packet implements Serializable {

    private Type type;
    private String message;

    public Packet() {
        this(Type.NONE, "");
    }

    public Packet(Type type, String message) {
        this.type = type;
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public enum Type {
            MESSAGE,
            INFO,
            NONE
    }
}
