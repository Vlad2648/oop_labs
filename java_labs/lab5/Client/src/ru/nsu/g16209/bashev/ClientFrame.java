package ru.nsu.g16209.bashev;

import ru.snu.g16209.Packet;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.Socket;

public class ClientFrame extends JFrame {

    public static final String SERVER_HOST = "localhost";
    public static final int SERVER_PORT = 3443;

    private Socket clientSocket;
    private ObjectInputStream inMessage;
    private ObjectOutputStream outMessage;

    private String name = "no name";
    private boolean isConnect = false;
    private ConnectThread connectThread;

    private JTextField messageField;
    private JTextField nameField;
    private JTextArea messagesArea;
    private JButton sendButton;
    private JLabel infoLabel;

    ClientFrame() {
        super("Чат");

        setSize(600, 500);

        //Созданик поля для сообщений
        messagesArea = new JTextArea();
        messagesArea.setEditable(false);
        messagesArea.setLineWrap(true);
        JScrollPane scrollPane = new JScrollPane(messagesArea);
        add(scrollPane, BorderLayout.CENTER);

        //Создание поля с количеством участников
        infoLabel = new JLabel("Участников в чате: 0");
        add(infoLabel, BorderLayout.NORTH);

        //Создание нижней понели
        JPanel bottomPanel = new JPanel(new BorderLayout());
        //Создание поля с именем
        nameField = new JTextField("Введите ваше имя ");
        nameField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent focusEvent) {
                if (nameField.isEditable()) {
                    nameField.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                name = nameField.getText();
                sendInfo("!name:" + name);
                sendMessage("Новый участник вошел в чатб его имя: " + name);
                nameField.setEditable(false);
            }
        });
        bottomPanel.add(nameField, BorderLayout.WEST);
        //Создание поля с новым сообщением
        messageField = new JTextField("Введите ваше сообщение");
        messageField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent focusEvent) {
                messageField.setText("");
            }
        });
        bottomPanel.add(messageField, BorderLayout.CENTER);
        //Создание кнопки отправить
        sendButton = new JButton("Отправить");
        sendButton.addActionListener(e -> clickSend());
        bottomPanel.add(sendButton, BorderLayout.EAST);

        add(bottomPanel, BorderLayout.SOUTH);

        JRootPane rootPane = SwingUtilities.getRootPane(sendButton);
        rootPane.setDefaultButton(sendButton);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setResizable(true);
        setVisible(true);

        //Подключение к серверу
        connectToServer();

        //Создание нового потока-обработчика
        connectThread = new ConnectThread();
        connectThread.start();

        //Создание обраюотчика на закрытие окна
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                super.windowClosing(windowEvent);
                if (!isConnect) {
                    return;
                }
                if (name.equals("no name")) {
                    sendMessage("Участник вышел из чата, так и не представившись!");
                } else {
                    sendMessage(name + " вышел из чата");
                }
                sendInfo("@@@session@@@end@@@");


                try {
                    inMessage.close();
                    outMessage.close();
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                connectThread.interrupt();
                try {
                    connectThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void connectToServer() {
        messagesArea.append("Подключение к серверу...\n");
        while (true) {
            try {
                clientSocket = new Socket(SERVER_HOST, SERVER_PORT);
            } catch (IOException e) {
                continue;
            }
            break;
        }

        try {
            outMessage = new ObjectOutputStream(clientSocket.getOutputStream());
            inMessage = new ObjectInputStream(clientSocket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        messagesArea.append("Сервер подключен.\n");
        isConnect = true;
    }

    private void clickSend() {
        if (!isConnect) {
            return;
        }
        if (name.equals("no name")) {
            messagesArea.append("Введите ваше имя\n");
            return;
        }
        if (messageField.getText().equals("!who")) {
            sendInfo("!who");
            messageField.setText("");
            messageField.grabFocus();
            return;
        }
        if (!name.trim().isEmpty() && !messageField.getText().trim().isEmpty()) {
            sendMessage(name + ": " + messageField.getText());
            messageField.setText("");
            messageField.grabFocus();
        }
    }

    private void sendMessage(String message) {
        System.out.println("New message: " + message);
        Packet packet = new Packet(Packet.Type.MESSAGE, message);
        try {
            outMessage.writeObject(packet);
            outMessage.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendInfo(String info) {
        Packet packet = new Packet(Packet.Type.INFO, info);
        try {
            outMessage.writeObject(packet);
            outMessage.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void infoHandle(Packet packet) {
        if (packet.getMessage().indexOf("Участников в чате: ") == 0) {
            infoLabel.setText(packet.getMessage());
        }
    }

    private void messageHandle(Packet packet) {
        messagesArea.append(packet.getMessage() + "\n");
    }

    @SuppressWarnings("Duplicates")
    private class ConnectThread extends Thread {
        boolean interrupt = false;

        @Override
        public void run() {
            Packet message;
            try {
                while (!interrupt) {
                    if ((message = (Packet) inMessage.readObject()) != null) {
                        if (message.getType() == Packet.Type.INFO) {
                            infoHandle(message);
                        } else {
                            messageHandle(message);
                        }
                    }
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }

        @Override
        public void interrupt() {
            interrupt = true;
            super.interrupt();
        }
    }
}
