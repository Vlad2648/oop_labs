package ru.nsu.g16209.bashev;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Server extends Thread{

    private static final Logger log = Logger.getLogger(Server.class.getName());

    private static final int PORT = 3443;
    private List<ClientHandler> clients = new ArrayList<ClientHandler>();
    private List<String> messageHistory = new ArrayList<String>();

    private ServerSocket serverSocket;

    private boolean interrupt = false;

    public Server() {
        startSever();
    }

    @Override
    public void run() {
        log.info("Server started");
        Socket clientSocket = null;
        ClientHandler clientHandler = null;
        while (!interrupt) {
            try {
                clientSocket = serverSocket.accept();
                log.info("New client connected");
                clientHandler = new ClientHandler(clientSocket, this);
                clients.add(clientHandler);
                clientHandler.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        stopServer();
        log.info("Server stopped");
    }

    @Override
    public void interrupt() {
        interrupt = true;
        super.interrupt();
    }

    public void sendMessageToAllClients(String message) {
        messageHistory.add(message);
        for (ClientHandler clientHandler : clients) {
            clientHandler.sendMessage(message);
        }
    }

    public void sendInfoToAllClients(String message) {
        for (ClientHandler clientHandler : clients) {
            clientHandler.sendInfo(message);
        }
    }

    public void sendHistory(ClientHandler clientHandler) {
        for (String s : messageHistory) {
            clientHandler.sendMessage(s);
        }
    }

    public void getMembers(ClientHandler clientHandler) {
        for (ClientHandler ch : clients) {
            clientHandler.sendMessage(ch.getClientName());
        }
    }

    public void removeClient(ClientHandler clientHandler) {
        log.info("Client disconnect");
        clients.remove(clientHandler);
    }

    private void startSever() {
        try {
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stopServer() {
        for (ClientHandler clientHandler : clients) {
            clientHandler.interrupt();
        }

        try {
            for (ClientHandler clientHandler : clients) {
                clientHandler.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
