package ru.nsu.g16209.bashev;

import ru.snu.g16209.Packet;

import java.io.*;
import java.net.Socket;
import java.util.logging.Logger;

public class ClientHandler extends Thread {

    private static final Logger log = Logger.getLogger(ClientHandler.class.getName());

    private Server server;
    private Socket socket;

    private ObjectInputStream inMessage;
    private ObjectOutputStream outMessage;

    private static int clientCount = 0;
    private String clientName = "no name";

    private boolean interrupt = false;

    ClientHandler(Socket socket, Server server) {
        this.server = server;
        this.socket = socket;
        clientCount++;

        try {
            outMessage = new ObjectOutputStream(socket.getOutputStream());
            inMessage = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @SuppressWarnings("Duplicates")
    public void run() {
        log.info("Start processing new client");

        server.sendInfoToAllClients("Участников в чате: " + clientCount);
        server.sendHistory(this);

        Packet message;
        try {
            while (!interrupt) {
                if ((message = (Packet) inMessage.readObject()) != null) {
                    log.info("New message received");

                    if (message.getType() == Packet.Type.INFO) {
                        infoHandle(message);
                    } else {
                        messageHandle(message);
                    }
                }
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        closeClient();
        log.info("Stop processing client");
    }

    private void infoHandle(Packet packet) {
        String command = packet.getMessage();
        if (command.equals("!who")) {
            server.getMembers(this);
        } else if (command.equals("@@@session@@@end@@@")) {
            interrupt = true;
        } else if (command.indexOf("!name:") == 0) {
            String[] words = command.split(":");
            clientName = words[1];
        } else {
            sendMessage("Неизвестная команда");
        }
    }

    private void messageHandle(Packet packet) {
        server.sendMessageToAllClients(packet.getMessage());
    }

    @Override
    public void interrupt() {
        interrupt = true;
        super.interrupt();
    }

    public void sendMessage(String message) {
        Packet packet = new Packet(Packet.Type.MESSAGE, message);
        try {
            outMessage.writeObject(packet);
            outMessage.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendInfo(String info) {
        Packet packet = new Packet(Packet.Type.INFO, info);
        try {
            outMessage.writeObject(packet);
            outMessage.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getClientName() {
        return clientName;
    }

    private void closeClient() {
        server.removeClient(this);
        clientCount--;
        try {
            inMessage.close();
            outMessage.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
