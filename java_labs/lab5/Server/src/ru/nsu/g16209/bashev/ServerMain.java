package ru.nsu.g16209.bashev;

import java.io.IOException;

public class ServerMain {

    public static final int DEFAULT_PORT = 11122;

    public static void main(String[] args) {
        Server server = new Server();
        server.start();
        System.out.println("Server started, write something to stop it");
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        server.interrupt();
        try {
            server.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Server stopped");
    }
}
