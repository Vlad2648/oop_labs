package ru.nsu.g16209.commands;

import org.junit.jupiter.api.Test;
import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CommandAddTest {

    @Test
    void executeCorrectPlus() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();
        stack.add(2.0);
        stack.add(3.0);

        CommandAdd commandAdd = new CommandAdd();
        try {
            commandAdd.execute(context);
        } catch (Exception e) {
            assertTrue(false);
        }

        assertEquals(stack.get(0).doubleValue(), 5.0);
    }

    @Test
    void executeCorrectMinus() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();
        stack.add(-2.0);
        stack.add(-2.0);

        CommandAdd commandAdd = new CommandAdd();
        try {
            commandAdd.execute(context);
        } catch (Exception e) {
            assertTrue(false);
        }

        assertEquals(stack.get(0).doubleValue(), -4.0);
    }

    @Test
    void executeCorrectZero() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();
        stack.add(2.0);
        stack.add(-2.0);

        CommandAdd commandAdd = new CommandAdd();
        try {
            commandAdd.execute(context);
        } catch (Exception e) {
            assertTrue(false);
        }

        assertEquals(stack.get(0).doubleValue(), 0.0);
    }

    @Test
    void executeCorrectThrow() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();

        CommandAdd commandAdd = new CommandAdd();
        assertThrows(Exception.class, () -> commandAdd.execute(context));
    }
}