package ru.nsu.g16209.commands;

import org.junit.jupiter.api.Test;
import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CommandPopTest {

    @Test
    void executeCorrect() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack = context.getStack();
        stack.add(5.0);

        CommandPop commandPop = new CommandPop();

        try {
            commandPop.execute(context);
        } catch (Exception e) {
            assertTrue(false);
        }
        assertTrue(stack.isEmpty());
    }

    @Test
    void executeThrow() {
        CalculatorContext context = new CalculatorContext();

        CommandPop commandPop = new CommandPop();
        assertThrows(Exception.class, () -> commandPop.execute(context));
    }
}