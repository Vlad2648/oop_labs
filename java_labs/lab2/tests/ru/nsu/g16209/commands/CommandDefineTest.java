package ru.nsu.g16209.commands;

import org.junit.jupiter.api.Test;
import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CommandDefineTest {

    @Test
    void initArgsCorrect() {
        CommandDefine commandDefine = new CommandDefine();
        String[] args = {"DEFINE", "AAA", "5.0"};
        try {
            commandDefine.initArgs(args);
            assertTrue(true);
        } catch (Exception e) {
            assertTrue(false);
        }
    }

    @Test
    void initArgsThrowArgs() {
        CommandDefine commandDefine = new CommandDefine();
        String[] args = {"DEFINE", "AAA"};

        assertThrows(Exception.class, () -> commandDefine.initArgs(args));
    }

    @Test
    void initArgsThrowNum() {
        CommandDefine commandDefine = new CommandDefine();
        String[] args = {"DEFINE", "AAA", "BBB"};

        assertThrows(Exception.class, () -> commandDefine.initArgs(args));
    }

    @Test
    void executeCorrect() {
        CalculatorContext context = new CalculatorContext();
        Map<String, Double> defines = context.getDefines();

        CommandDefine commandDefine = new CommandDefine();
        String[] args = {"DEFINE", "AAA", "5.0"};

        try {
            commandDefine.initArgs(args);
            commandDefine.execute(context);
        } catch (Exception e) {
            assertTrue(false);
        }

        assertEquals(defines.get("AAA").doubleValue(), 5.0);
    }
}