package ru.nsu.g16209.commands;

import org.junit.jupiter.api.Test;
import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CommandMultTest {

    @Test
    void executeCorrectPlus() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();
        stack.add(2.0);
        stack.add(2.0);

        CommandMult commandMult = new CommandMult();
        try {
            commandMult.execute(context);
        } catch (Exception e) {
            assertTrue(false);
        }

        assertEquals(stack.get(0).doubleValue(), 4.0);
    }

    @Test
    void executeCorrectMinus() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();
        stack.add(-2.0);
        stack.add(-2.0);

        CommandMult commandMult = new CommandMult();
        try {
            commandMult.execute(context);
        } catch (Exception e) {
            assertTrue(false);
        }

        assertEquals(stack.get(0).doubleValue(), 4.0);
    }

    @Test
    void executeCorrectZero() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();
        stack.add(2.0);
        stack.add(0.0);

        CommandMult commandMult = new CommandMult();
        try {
            commandMult.execute(context);
        } catch (Exception e) {
            assertTrue(false);
        }

        assertEquals(stack.get(0).doubleValue(), 0.0);
    }

    @Test
    void executeCorrectThrow() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();

        CommandMult commandMult = new CommandMult();
        assertThrows(Exception.class, () -> commandMult.execute(context));
    }
}