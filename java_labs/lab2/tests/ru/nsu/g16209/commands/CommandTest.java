package ru.nsu.g16209.commands;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CommandTest {

    @Test
    void getNumberCorrect() {
        Map<String, Double> defines = new HashMap<>();
        try {
            assertEquals(Command.getNumber("5.0", defines), 5.0);
        } catch (Exception e) {
            assertTrue(false);
        }
    }

    @Test
    void getNumberCorrectDefine() {
        Map<String, Double> defines = new HashMap<>();
        defines.put("AAA", 5.0);
        try {
            assertEquals(Command.getNumber("AAA", defines), 5.0);
        } catch (Exception e) {
            assertTrue(false);
        }
    }

    @Test
    void getNumberThrow() {
        Map<String, Double> defines = new HashMap<>();
        assertThrows(Exception.class, () -> Command.getNumber("AAA", defines));
    }
}