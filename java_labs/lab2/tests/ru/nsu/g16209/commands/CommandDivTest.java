package ru.nsu.g16209.commands;

import org.junit.jupiter.api.Test;
import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CommandDivTest {

    @Test
    void executeCorrect() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();
        stack.add(2.0);
        stack.add(2.0);

        CommandDiv commandDiv = new CommandDiv();
        try {
            commandDiv.execute(context);
        } catch (Exception e) {
            assertTrue(false);
        }

        assertEquals(stack.get(0).doubleValue(), 1.0);
    }

    @Test
    void executeThrowArgs() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();
        stack.add(2.0);

        CommandDiv commandDiv = new CommandDiv();
        assertThrows(Exception.class, () -> commandDiv.execute(context));
    }

    @Test
    void executeThrowDivByZero() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();
        stack.add(0.0);
        stack.add(2.0);

        CommandDiv commandDiv = new CommandDiv();
        assertThrows(Exception.class, () -> commandDiv.execute(context));
    }
}