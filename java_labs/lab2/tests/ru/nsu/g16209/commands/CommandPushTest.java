package ru.nsu.g16209.commands;

import org.junit.jupiter.api.Test;
import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CommandPushTest {

    @Test
    void initArgsCorrect() {
        CommandPush commandPush = new CommandPush();
        String[] args = {"PUSH", "AAA"};
        try {
            commandPush.initArgs(args);
            assertTrue(true);
        } catch (Exception e) {
            assertTrue(false);
        }
    }

    @Test
    void initArgsThrowArgs() {
        CommandPush commandPush = new CommandPush();
        String[] args = {"PUSH"};

        assertThrows(Exception.class, () -> commandPush.initArgs(args));
    }

    @Test
    void executeCorrect() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack = context.getStack();

        CommandPush commandPush = new CommandPush();
        String[] args = {"PUSH", "5.0"};

        try {
            commandPush.initArgs(args);
            commandPush.execute(context);
        } catch (Exception e) {
            assertTrue(false);
        }

        assertEquals(stack.get(0).doubleValue(), 5.0);
    }
}