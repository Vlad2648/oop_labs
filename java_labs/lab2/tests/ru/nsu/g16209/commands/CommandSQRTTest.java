package ru.nsu.g16209.commands;

import org.junit.jupiter.api.Test;
import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CommandSQRTTest {

    @Test
    void executeCorrect() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();
        stack.add(4.0);

        CommandSQRT commandSQRT = new CommandSQRT();

        try {
            commandSQRT.execute(context);
        } catch (Exception e) {
            assertTrue(false);
        }

        assertEquals(stack.get(0).doubleValue(), 2.0);
    }

    @Test
    void executeThrowArgs() {
        CalculatorContext context = new CalculatorContext();

        CommandSQRT commandSQRT = new CommandSQRT();

        assertThrows(Exception.class, () -> commandSQRT.execute(context));
    }

    @Test
    void executeThrowMinus() {
        CalculatorContext context = new CalculatorContext();
        List<Double> stack= context.getStack();
        stack.add(-4.0);

        CommandSQRT commandSQRT = new CommandSQRT();

        assertThrows(Exception.class, () -> commandSQRT.execute(context));
    }
}