package ru.nsu.g16209.factory;

import org.junit.jupiter.api.Test;
import ru.nsu.g16209.commands.Command;

import static org.junit.jupiter.api.Assertions.*;

class CommandFactoryTest {

    private Command createCommand(String[] args) {
        CommandFactory factory = new CommandFactory();

        Command command = null;
        try {
            command = factory.createCommand(args[0], args);
        } catch (Exception e) {
            assertTrue(false);
        }
        return command;
    }

    @Test
    void createCommandUnknownThrows() {
        String[] str = {"AAAAA"};

        CommandFactory factory = new CommandFactory();
        assertThrows(Exception.class, () -> factory.createCommand(str[0], str));
    }

    @Test
    void createCommandPop() {
        String[] str = {"POP"};

        Command command = createCommand(str);

        String expected = "ru.nsu.g16209.commands.CommandPop";
        assertEquals(command.getClass().getName(), expected);
    }

    @Test
    void createCommandPush() {
        String[] str = {"PUSH", "5"};

        Command command = createCommand(str);

        String expected = "ru.nsu.g16209.commands.CommandPush";
        assertEquals(command.getClass().getName(), expected);
    }

    @Test
    void createCommandPushThrows() {
        String[] str = {"PUSH"};

        CommandFactory factory = new CommandFactory();
        assertThrows(Exception.class, () -> factory.createCommand(str[0], str));
    }

    @Test
    void createCommandAdd() {
        String[] str = {"+"};

        Command command = createCommand(str);

        String expected = "ru.nsu.g16209.commands.CommandAdd";
        assertEquals(command.getClass().getName(), expected);
    }

    @Test
    void createCommandSub() {
        String[] str = {"-"};

        Command command = createCommand(str);

        String expected = "ru.nsu.g16209.commands.CommandSub";
        assertEquals(command.getClass().getName(), expected);
    }

    @Test
    void createCommandMult() {
        String[] str = {"*"};

        Command command = createCommand(str);

        String expected = "ru.nsu.g16209.commands.CommandMult";
        assertEquals(command.getClass().getName(), expected);
    }

    @Test
    void createCommandDiv() {
        String[] str = {"/"};

        Command command = createCommand(str);

        String expected = "ru.nsu.g16209.commands.CommandDiv";
        assertEquals(command.getClass().getName(), expected);
    }

    @Test
    void createCommandSQRT() {
        String[] str = {"SQRT"};

        Command command = createCommand(str);

        String expected = "ru.nsu.g16209.commands.CommandSQRT";
        assertEquals(command.getClass().getName(), expected);
    }

    @Test
    void createCommandPrint() {
        String[] str = {"PRINT"};

        Command command = createCommand(str);

        String expected = "ru.nsu.g16209.commands.CommandPrint";
        assertEquals(command.getClass().getName(), expected);
    }

    @Test
    void createCommandDefine() {
        String[] str = {"DEFINE", "a", "5"};

        Command command = createCommand(str);

        String expected = "ru.nsu.g16209.commands.CommandDefine";
        assertEquals(command.getClass().getName(), expected);
    }

    @Test
    void createCommandDefineThrows() {
        String[] str = {"DEFINE"};

        CommandFactory factory = new CommandFactory();
        assertThrows(Exception.class, () -> factory.createCommand(str[0], str));

    }
}