package ru.nsu.g16209.factory;

import ru.nsu.g16209.commands.Command;

import java.io.InputStream;
import java.util.Scanner;
import java.util.logging.Logger;

public class CommandFactory {

    private final String configName = "config";

    private InputStream inputStream;

    private Scanner scan;

    private static Logger log = Logger.getLogger(Command.class.getName());

    public CommandFactory() {

    }

    private void initStreams() {
        inputStream = CommandFactory.class.getResourceAsStream(configName);
        scan = new Scanner(inputStream).useDelimiter("[ \n]");
    }

    private void closeStreams() {
        scan.close();
        try {
            inputStream.close();
        } catch (Exception e) {
            log.info("Something wrong with closing input stream");
        }
    }

    public Command createCommand(String command, String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Command commandObj = newCommand(command);
        commandObj.initArgs(args);
        log.info("Command \"" + command + "\" object successful created and initiated");
        return commandObj;
    }

    private Command newCommand(String command) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        initStreams();
        String line;
        String commandClassName = null;

        while (scan.hasNext()) {
            line = scan.next();

            if (line.equals(command)) {
                commandClassName = scan.next();
                break;
            }
        }
        closeStreams();
        
        if (commandClassName == null) {
            log.info("Can't find command \"" + command + "\"");
            throw new ClassNotFoundException("Can't find command class");
        }

        Class commandClass = Class.forName(commandClassName);
        return (Command)commandClass.newInstance();
    }

}
