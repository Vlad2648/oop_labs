package ru.nsu.g16209.main;

import ru.nsu.g16209.calculator.Calculator;

import java.io.*;
import java.util.logging.LogManager;


public class Main {

    public static void main(String[] args) {

        try{
            LogManager.getLogManager().readConfiguration(Main.class.getResourceAsStream("/logging.properties"));
        } catch (IOException e) {
            System.out.println("Couldn't setup login configuration");
        }

        Calculator calculator = null;

        if (args.length == 1) {
            try {
                File file = new File(args[0]);
                InputStream inputStream = new FileInputStream(file);
                calculator = new Calculator(inputStream);
            } catch (FileNotFoundException e) {
                System.out.println("File not found");
            }
        } else {
            calculator = new Calculator(System.in);
        }

        try {
            calculator.calculate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
