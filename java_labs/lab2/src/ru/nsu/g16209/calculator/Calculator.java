package ru.nsu.g16209.calculator;

import ru.nsu.g16209.commands.Command;
import ru.nsu.g16209.factory.CommandFactory;

import java.io.InputStream;
import java.util.Scanner;
import java.util.logging.Logger;

public class Calculator {

    private CommandFactory factory;

    private CalculatorContext context;

    private Scanner scan;

    private static Logger log = Logger.getLogger(Calculator.class.getName());

    public Calculator(InputStream inputStream) {
        factory = new CommandFactory();
        context = new CalculatorContext();
        scan = new Scanner(inputStream);
    }

    public void calculate() {
        String line;
        String[] words;
        final String delimiter = "[ ]+";

        Command currentCommand;

        while (scan.hasNextLine()) {
            line = scan.nextLine();
            if (line.equals("")) continue;
            words = line.split(delimiter);
            if (words[0].startsWith("#")) continue;
            try {
                currentCommand = factory.createCommand(words[0], words);
                currentCommand.execute(context);
                log.info("Line: \"" + line + "\" success");
            } catch (IllegalAccessException e) {
                log.info("Wrong line: \"" + line + "\" " + e);
            } catch (ClassNotFoundException e) {
                log.info("Wrong line: \"" + line + "\" " + e);
                //throw new Exception("Wrong line: \"" + line + "\" " + e);
            } catch (InstantiationException e) {
                log.info("Wrong line: \"" + line + "\" " + e);
            } catch (ArithmeticException e) {
                log.info("Wrong line: \"" + line + "\" " + e);
            }
        }
    }
}
