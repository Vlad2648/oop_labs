package ru.nsu.g16209.calculator;

import java.util.*;

public class CalculatorContext {

    private List<Double> stack;

    private Map<String, Double> defines;

    public CalculatorContext() {
        stack = new ArrayList<>();
        defines = new HashMap<>();
    }

    public List<Double> getStack() {
        return stack;
    }

    public Map<String, Double> getDefines() {
        return defines;
    }
}
