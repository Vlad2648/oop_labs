package ru.nsu.g16209.commands;

import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.logging.Logger;

public class CommandPop extends Command {

    private static Logger log = Logger.getLogger(CommandPop.class.getName());

    public CommandPop() {

    }

    public void initArgs(String[] args) throws IllegalAccessException {

    }

    public void execute(CalculatorContext context) throws ArithmeticException, IllegalAccessException {
        context.getStack().remove(0);
        log.info("Last number removed from stack");
    }
}
