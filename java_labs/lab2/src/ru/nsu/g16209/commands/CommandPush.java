package ru.nsu.g16209.commands;

import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.logging.Logger;

public class CommandPush extends Command {

    private String value;

    private static Logger log = Logger.getLogger(CommandPush.class.getName());

    public CommandPush() {

    }

    public void initArgs(String[] args) throws IllegalAccessException {
        if (args.length < 2) {
            log.info("Invalid number of argument");
            throw new IllegalAccessException("Push: Invalid number of args");
        }
        value = args[1];
        log.info("Initiated successful");
    }

    public void execute(CalculatorContext context) throws ArithmeticException, IllegalAccessException {
        double num = Command.getNumber(value, context.getDefines());
        context.getStack().add(0, num);
        log.info("Number: " + num + "add to stack");
    }
}
