package ru.nsu.g16209.commands;

import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.Map;

public abstract class Command {

    public abstract void initArgs(String[] args) throws IllegalAccessException;

    public abstract void execute(CalculatorContext context) throws ArithmeticException, IllegalAccessException;

    public static double getNumber(String value, Map<String, Double> defines) throws IllegalAccessException{
        Double num;
        if ((num = defines.get(value)) != null) {
            return num;
        }
        try {
            num = Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new IllegalAccessException(e.getMessage());
        }
        return num;
    }

}
