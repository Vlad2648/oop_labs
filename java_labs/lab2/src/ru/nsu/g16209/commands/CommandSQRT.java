package ru.nsu.g16209.commands;

import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.List;
import java.util.logging.Logger;

public class CommandSQRT extends Command {

    private static Logger log = Logger.getLogger(CommandSQRT.class.getName());

    public CommandSQRT() {

    }

    public void initArgs(String[] args) throws IllegalAccessException {

    }

    public void execute(CalculatorContext context) throws ArithmeticException, IllegalAccessException {
        List<Double> stack = context.getStack();
        if (stack.isEmpty()) {
            log.info("Stack is empty");
            throw new IllegalAccessException("Invalid number in stack");
        }

        double value = stack.get(0);

        if (value < 0) {
            log.info("Number less that 0");
            throw new ArithmeticException("SQRT: Value < 0");
        }

        stack.remove(0);
        log.info("Last number removed from stack");

        stack.add(0, Math.sqrt(value));
        log.info("Result add to stack");
    }
}
