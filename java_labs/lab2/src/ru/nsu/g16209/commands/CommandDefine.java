package ru.nsu.g16209.commands;

import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.logging.Logger;

public class CommandDefine extends Command {

    private String def;

    private double num;

    private static Logger log = Logger.getLogger(CommandDefine.class.getName());

    public CommandDefine() {

    }

    public void initArgs(String[] args) throws IllegalAccessException {
        if (args.length < 3) {
            throw new IllegalAccessException("Define: Invalid number of args");
        }

        def = args[1];
        num = Double.parseDouble(args[2]);
        log.info("Arguments initiated");
    }

    public void execute(CalculatorContext context) throws ArithmeticException, IllegalAccessException {
        context.getDefines().put(def, num);
        log.info("Define: " + def + " with number " + num + "add to defines");
    }
}
