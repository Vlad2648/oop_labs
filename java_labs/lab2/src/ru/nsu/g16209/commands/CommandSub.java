package ru.nsu.g16209.commands;

import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.List;
import java.util.logging.Logger;

public class CommandSub extends Command {

    private static Logger log = Logger.getLogger(CommandSub.class.getName());

    public CommandSub() {

    }

    public void initArgs(String[] args) throws IllegalAccessException {

    }

    public void execute(CalculatorContext context) throws ArithmeticException, IllegalAccessException {
        List<Double> stack = context.getStack();
        if (stack.size() < 2) {
            log.info("Invalide number of arguments in stack");
            throw new IllegalAccessException("Sub: Invalid number in stack");
        }

        double firstValue = stack.get(0);
        stack.remove(0);
        log.info("Number: " + firstValue + "removed from stack");
        double secondValue = stack.get(0);
        stack.remove(0);
        log.info("Number: " + secondValue + "removed from stack");

        stack.add(0, secondValue - firstValue);
        log.info("Number: " + (secondValue - firstValue) + "add to stack");
    }
}
