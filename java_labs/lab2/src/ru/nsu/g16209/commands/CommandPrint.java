package ru.nsu.g16209.commands;

import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.List;
import java.util.logging.Logger;

public class CommandPrint extends Command {

    private static Logger log = Logger.getLogger(CommandPrint.class.getName());

    public CommandPrint() {

    }

    public void initArgs(String[] args) throws IllegalAccessException {

    }

    public void execute(CalculatorContext context) throws ArithmeticException, IllegalAccessException {
        List<Double> stack = context.getStack();
        if (stack.isEmpty()) {
            log.info("Stack is empty");
            throw new IllegalAccessException("Print: Invalid number in stack");
        }

        double value = stack.get(0);
        System.out.println(value);
        log.info("Last value successful printed");
    }
}
