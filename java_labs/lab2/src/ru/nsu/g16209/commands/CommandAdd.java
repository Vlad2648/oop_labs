package ru.nsu.g16209.commands;

import ru.nsu.g16209.calculator.CalculatorContext;

import java.util.List;
import java.util.logging.Logger;

public class CommandAdd extends Command {

    private static Logger log = Logger.getLogger(CommandAdd.class.getName());

    public CommandAdd() {

    }

    public void initArgs(String[] args) throws IllegalAccessException {

    }

    public void execute(CalculatorContext context) throws ArithmeticException, IllegalAccessException {
        List<Double> stack = context.getStack();
        if (stack.size() < 2) {
            log.info("Invalid number in stack");
            throw new IllegalAccessException("Add: Invalid number in stack");
        }

        double firstValue = stack.get(0);
        stack.remove(0);
        log.info("Number: " + firstValue + "removed from stack");
        double secondValue = stack.get(0);
        stack.remove(0);
        log.info("Number: " + secondValue + "removed from stack");

        stack.add(0, firstValue + secondValue);
        log.info("Number: " + (firstValue + secondValue) + "add to stack");
    }
}
