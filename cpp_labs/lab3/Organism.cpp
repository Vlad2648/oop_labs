//
// Created by vlad on 25.10.17.
//

#include "Organism.h"

bool Organism::isAlive() const {
    return live;
}

void Organism::setLive(bool state) {
    this->live = state;
}

Organism &Organism::operator=(const Organism &copy) {
    this->live = copy.live;
    return *this;
}
