//
// Created by vlad on 25.10.17.
//

#ifndef LAB3_GRID_H
#define LAB3_GRID_H

#include "Cell.h"
#include <iostream>
#include <vector>
#include <iterator>

class Grid {
public:
    //Default constructor
    Grid();

    //Constructor with width and height
    Grid(size_t width, size_t height);

    //Set live state in position (x, y)
    void setXYState(size_t x, size_t y, bool state);

    //Refresh grid to revise live
    void refreshGrid();

    //Print all grid
    void printGrid() const;

    void createNewGrid(size_t width, size_t height);

    Grid& operator= (const Grid& copy);

    bool operator== (const Grid& grid);

protected:
    /*
     * Find neighbor to Cell[x][y] with position pos in range:
     * 1 2 3
     * 4   5
     * 6 7 8
     */
    bool isPosNeighborAlive(size_t x, size_t y, size_t pos) const;

    size_t countAliveNeighbor(size_t x, size_t y);

private:
    size_t gridWidth = 10;
    size_t gridHeight = 10;
    std::vector<std::vector<Cell>> gMatrix;

    std::vector<std::vector<size_t >> queue;

    friend std::ostream& operator <<(std::ostream& os, const Grid& grid);
};

std::ostream& operator<<(std::ostream& os, const Grid& grid);

#endif //LAB3_GRID_H
