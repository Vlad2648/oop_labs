//
// Created by vlad on 25.10.17.
//

#include "Grid.h"

Grid::Grid() {
    gMatrix = std::vector<std::vector<Cell>>(gridWidth);
    for(size_t i = 0; i < gridWidth; i++) {
        gMatrix[i] = std::vector<Cell>(gridHeight);
    }
}

Grid::Grid(size_t width, size_t height) {
    this->gridWidth = width;
    this->gridHeight = height;
    gMatrix = std::vector<std::vector<Cell>>(gridWidth);
    for(size_t i = 0; i < gridWidth; i++) {
        gMatrix[i] = std::vector<Cell>(gridHeight);
    }
}

void Grid::setXYState(size_t x, size_t y, bool state) {
    gMatrix[x][y].setLive(state);
}

void Grid::refreshGrid() {
    size_t count;
    bool live;
    for (size_t i = 0; i < gridWidth; ++i) {
        for (size_t j = 0; j < gridHeight; ++j) {
            live = gMatrix[i][j].isAlive();
            count = countAliveNeighbor(i, j);
            if (live && (count < 2 || count > 3)) {
                std::vector<size_t> v = { i, j , 0 };
                queue.emplace_back(v);
            } else {
                if (!live && count == 3) {
                    std::vector<size_t> v = {i, j, 1};
                    queue.emplace_back(v);
                }
            }
        }
    }
    for(auto temp : queue) {
        if(temp[2]) {
            gMatrix[temp[0]][temp[1]].setLive(true);
        } else {
            gMatrix[temp[0]][temp[1]].setLive(false);
        }
    }
    queue.clear();
}

void Grid::printGrid() const {
    std::cout << std::endl;
    for(auto &i : gMatrix) {
        for(auto &j : i) {
            std::cout << j.takeEntityForm() << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

bool Grid::isPosNeighborAlive(size_t x, size_t y, size_t pos) const {
    auto dx = (int) x;
    auto dy = (int) y;
    switch (pos) {
        case 1: case 4: case 6:
            --dx;
            break;
        case 3: case 5: case 8:
            ++dx;
            break;
        case 2: case 7:
            break;
        default:
            return false;
    };
    switch (pos) {
        case 1: case 2: case 3:
            --dy;
            break;
        case 6: case 7: case 8:
            ++dy;
            break;
        case 4: case 5:
            break;
    };
    dx = dx < 0 ? (((int) gridWidth) - 1) : dx;
    dy = dy < 0 ? (((int) gridHeight) - 1) : dy;
    dx %= gridWidth;
    dy %= gridHeight;
    return gMatrix[dx][dy].isAlive();
}

size_t Grid::countAliveNeighbor(size_t x, size_t y) {
    size_t result = 0;
    for(size_t k = 1; k < 9; ++k) {
        if(isPosNeighborAlive(x, y, k)) {
            ++result;
        }
    }
    return result;
}

void Grid::createNewGrid(size_t width, size_t height) {
    gridWidth = width;
    gridHeight = height;
    gMatrix = std::vector<std::vector<Cell>>(gridWidth);
    for(size_t i = 0; i < gridWidth; i++) {
        gMatrix[i] = std::vector<Cell>(gridHeight);
    }
}

Grid &Grid::operator= (const Grid &copy) {
        this->createNewGrid(copy.gridWidth, copy.gridHeight);
        this->gMatrix = copy.gMatrix;
    return *this;
}

bool Grid::operator== (const Grid &grid) {
    if((this->gridWidth != grid.gridWidth) || (this->gridHeight != grid.gridHeight)) {
        return false;
    }
    for(size_t i = 0; i < gridWidth; ++i) {
        for(size_t j = 0; j < gridHeight; ++j) {
            if (this->gMatrix[i][j].isAlive() != grid.gMatrix[i][j].isAlive()) {
                return false;
            }
        }
    }
    return true;
}

std::ostream& operator<<(std::ostream& os, const Grid& grid) {
    os << grid.gridWidth << std::endl;
    os << grid.gridHeight << std::endl;
    for (auto tempArr : grid.gMatrix) {
        for(auto temp : tempArr) {
            os << temp.takeEntityForm();
        }
        os << std::endl;
    }
    return os;
}

