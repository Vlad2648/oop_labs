//
// Created by vlad on 02.11.17.
//

#ifndef LAB3_GAME_H
#define LAB3_GAME_H

#include <iostream>
#include <fstream>
#include <thread>

#include "Grid.h"


class Game {
public:
    //Default constructor
    Game();

    Game(size_t width, size_t height);

    //Create new empty grid
    void resetGrid();

    //Born new life in position (x, y)
    void bornEntity(size_t x, size_t y);

    //Kill life in position (x, y)
    void killEntity(size_t x, size_t y);

    //Run simulation for step iteration
    void iterationStep(size_t step);

    //Go back if it possible or write error
    void goBack();

    //Save game into file with name fileName
    void saveGame(std::string fileName);

    //Load game from file with fileName
    void loadGame(std::string fileName);

    //Fill grid with first example
    void firstExample();

    //Finish game loop
    void stopGame();

    //Start game loop
    void startGame();

    //Return command number in order of commands or 0 if it isn't
    static size_t commandNum(std::string str);

    bool operator== (const Game& game);

private:
    //Recognize command and execute it
    void findAndExecuteCommand();

    size_t gameWidth = 10;
    size_t gameHeight = 10;

    Grid currentGrid;
    Grid previousGrid;

    bool isUndo = false;

    size_t iterator = 0;
    bool repeat = false;

};

#endif //LAB3_GAME_H
