#include <iostream>
#include "Game.h"

#include "tests.h"

int main(int argc, char* argv[]) {
    if(argc == 3)  {
        try {
            Game game(std::stoul(argv[1]), std::stoul(argv[2]));
            game.startGame();
        } catch (...) {
            std::cout << "Something wrong!!!!" << std::endl;
        }
    }
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}