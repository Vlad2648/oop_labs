//
// Created by vlad on 02.11.17.
//

#include "Game.h"

static const std::vector<std::string> command = {"reset", "set", "kill", "step", "back", "save", "load", "ex1", "exit"};

Game::Game() {
    currentGrid = Grid(gameWidth, gameHeight);
}

Game::Game(size_t width, size_t height) {
    gameWidth = width;
    gameHeight = height;
    currentGrid = Grid(gameWidth, gameHeight);
}

void Game::findAndExecuteCommand() {
    std::string str;
    std::getline(std::cin, str);
    size_t pos = 0;
    size_t xpos = 0;
    size_t ypos = 0;
    std::system("clear");
    switch (commandNum(str)) {
        case 1:
            resetGrid();
            break;
        case 2:
            try {
                xpos = std::stoul(str.substr(4), &pos, 10);
                pos += 4;
                ypos = std::stoul(str.substr(pos));
                bornEntity(xpos, ypos);
            } catch (...) {
                std::cout << "Can`t recognize the command" << std::endl;
            }
            break;
        case 3:
            try {
                xpos = std::stoul(str.substr(5), &pos, 10);
                pos += 5;
                ypos = std::stoul(str.substr(pos));
                killEntity(xpos, ypos);
            } catch (...) {
                std::cout << "Can`t recognize the command" << std::endl;
            }
            break;
        case 4:
            try {
                iterationStep(std::stoul(str.substr(4)));
            } catch (...) {
                std::cout << "Can`t recognize the command" << std::endl;
            }
            break;
        case 5:
            goBack();
            break;
        case 6:
            saveGame(str.substr(5));
            break;
        case 7:
            loadGame(str.substr(5));
            break;
        case 8:
            firstExample();
            break;
        case 9:
            stopGame();
            break;
        default:
            std::cout << "Can`t recognize the command" << std::endl;
            break;
    }
}

size_t Game::commandNum(std::string str) {
    for(size_t i = 0; i < command.size(); ++i) {
        if(command[i] == str.substr(0, command[i].size())) {
            return i + 1;
        }
    }
    return 0;
}

void Game::resetGrid() {
    currentGrid = Grid(gameWidth, gameHeight);
}

void Game::bornEntity(size_t x, size_t y) {
    currentGrid.setXYState(x % gameHeight, y % gameWidth, true);
}

void Game::killEntity(size_t x, size_t y) {
    currentGrid.setXYState(x % gameHeight, y % gameWidth, false);
}

void Game::iterationStep(size_t step) {
    for(iterator = step; iterator > 0; --iterator) {
        std::system("clear");
        previousGrid = currentGrid;
        currentGrid.refreshGrid();
        currentGrid.printGrid();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    isUndo = true;
    std::system("clear");
}

void Game::goBack() {
    if(isUndo) {
        currentGrid = previousGrid;
        isUndo = false;
    } else {
        std::cout << "Couldn`t go back" << std::endl;
    }
}

void Game::saveGame(std::string fileName) {
    std::ofstream file(fileName);
    if (!file.is_open()) {
        std::cout << "Can't open or create file \"" << fileName << "\"" << std::endl;
        return;
    }
    file << currentGrid << std::endl;
    file.close();
    std::cout << "Game save to \"" << fileName << "\"" << std::endl;
}

void Game::loadGame(std::string fileName) {
    std::ifstream file(fileName);
    if (!file.is_open()) {
        std::cout << "Can't open file \"" << fileName << "\"" << std::endl;
        return;
    }
    std::string line;
    try {
        std::getline(file, line);
        size_t width = std::stoul(line);
        std::getline(file, line);
        size_t height = std::stoul(line);
        currentGrid = Grid(width, height);
    } catch (...) {
        std::cout << "Something wrong in input file" << std::endl;
        return;
    }
    for(size_t i = 0; i < gameWidth; ++i) {
        std::getline(file, line);
        for(size_t j = 0; j < gameHeight; ++j) {
            if (line[j] == '*') {
                currentGrid.setXYState(i, j, true);
            } else {
                if (line[j] != '.') {
                    std::cout << "Something wrong in input file" << std::endl;
                    return;
                }
            }
        }
    }
    std::cout << "File successfully load from \"" << fileName << "\"" << std::endl;
}

void Game::firstExample() {
    resetGrid();
    bornEntity(1, 2);
    bornEntity(2, 3);
    bornEntity(3, 3);
    bornEntity(3, 2);
    bornEntity(3, 1);
}

void Game::stopGame() {
    repeat = false;
}

void Game::startGame() {
    repeat = true;
    std::system("clear");
    currentGrid.printGrid();
    while(repeat) {
        findAndExecuteCommand();
        currentGrid.printGrid();
    }
}

bool Game::operator==(const Game &game) {
    return this->currentGrid == game.currentGrid;
}
