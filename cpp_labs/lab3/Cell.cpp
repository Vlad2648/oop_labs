//
// Created by vlad on 25.10.17.
//

#include "Cell.h"

char Cell::takeEntityForm() const {
    return live ? '*' : '.';
}

void Cell::setLiveByForm(char sym) {
    if (sym == '*') {
        live = true;
    }
    if (sym == '.') {
        live = false;
    }
}
