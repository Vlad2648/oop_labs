//
// Created by vlad on 25.10.17.
//

#ifndef LAB3_CELL_H
#define LAB3_CELL_H
#include "Organism.h"

class Cell : public Organism {
public:
    char takeEntityForm() const;

    void setLiveByForm(char sym);
};


#endif //LAB3_CELL_H
