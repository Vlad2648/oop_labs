//
// Created by vlad on 25.10.17.
//

#ifndef LAB3_ORGANISM_H
#define LAB3_ORGANISM_H


class Organism {
public:
    Organism() = default;

    bool isAlive() const;

    void setLive(bool state);

    Organism& operator= (const Organism& copy);

protected:
    bool live = false;
};


#endif //LAB3_ORGANISM_H
