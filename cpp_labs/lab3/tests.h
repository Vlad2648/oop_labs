//
// Created by vlad on 27.11.17.
//

#ifndef LAB3_TESTS_H
#define LAB3_TESTS_H

#include "gtest/gtest.h"
#include "Game.h"

TEST(CellTests, Test_1) {
    Cell cell1;
    cell1.setLive(true);
    Cell cell2;
    cell2.setLive(false);
    ASSERT_TRUE((cell1.takeEntityForm() == '*') && (cell2.takeEntityForm() == '.'));
}
TEST(CellTests, Test_2) {
    Cell cell1;
    cell1.setLiveByForm('*');
    Cell cell2;
    cell2.setLiveByForm('.');
    Cell cell3;
    cell3.setLiveByForm('a');
    ASSERT_TRUE((cell1.isAlive() == true) && (cell2.isAlive() == false) && (cell3.isAlive() == false));
}
TEST(GridTests, Test_1) {
    Grid grid1;
    grid1.setXYState(1, 2, true);
    grid1.setXYState(2, 3, true);
    grid1.setXYState(3, 3, true);
    grid1.setXYState(3, 2, true);
    grid1.setXYState(3, 1, true);
    grid1.refreshGrid();
    Grid grid2;
    grid2.setXYState(2, 1, true);
    grid2.setXYState(2, 3, true);
    grid2.setXYState(3, 3, true);
    grid2.setXYState(3, 2, true);
    grid2.setXYState(4, 2, true);
    ASSERT_TRUE(grid1 == grid2);
}
TEST(GameTests, Test_1) {
    ASSERT_TRUE((Game::commandNum("reset") == 1) && (Game::commandNum("step 20") == 4) && (Game::commandNum("sfgdsfgsd") == 0));
}
TEST(GameTests, Test_2) {
    Game game1;
    game1.firstExample();
    game1.saveGame("tests.txt");
    Game game2;
    game2.loadGame("tests.txt");
    ASSERT_TRUE(game1 == game2);
}
#endif //LAB3_TESTS_H
