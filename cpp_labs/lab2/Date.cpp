//
// Created by vlad on 05.10.17.
//

#include "Date.h"


Date::Date() {
    time_d = time(NULL);
    localTime_d = localtime(&time_d);
    this->year = static_cast<unsigned>(localTime_d->tm_year + 1900);
    this->month = uintToMonth(static_cast<unsigned>(localTime_d->tm_mon + 1));
    this->day = static_cast<unsigned>(localTime_d->tm_mday);
    this->hour = static_cast<unsigned>(localTime_d->tm_hour);
    this->minute = static_cast<unsigned>(localTime_d->tm_min);
    this->second = static_cast<unsigned>(localTime_d->tm_sec);
}

Date::Date(const Date &obj) {
    this->year = obj.year;
    this->month = obj.month;
    this->day = obj.day;
    this->hour = obj.hour;
    this->minute = obj.minute;
    this->second = obj.second;
}

Date::Date(unsigned year, Date::Month month, unsigned day, unsigned hour, unsigned minute, unsigned second) {
    this->year = year;
    this->month = month;
    this->day = day;
    this->hour = hour;
    this->minute = minute;
    this->second = second;
    correctDate();
}

Date::Date(unsigned year, Date::Month month, unsigned day) {
    time_d = time(NULL);
    localTime_d = localtime(&time_d);
    this->year = year;
    this->month = month;
    this->day = day;
    this->hour = 0;
    this->minute = 0;
    this->second = 0;
    correctDate();
}

Date::Date(unsigned hour, unsigned minute, unsigned second) {
    time_d = time(NULL);
    localTime_d = localtime(&time_d);
    this->hour = hour;
    this->minute = minute;
    this->second = second;
    this->year = static_cast<unsigned>(localTime_d->tm_year + 1900);
    this->month = uintToMonth(static_cast<unsigned>(localTime_d->tm_mon + 1));
    this->day = static_cast<unsigned>(localTime_d->tm_mday);
    correctDate();
}

unsigned Date::monthToUInt(Date::Month value) {
    switch (value) {
        case Jan:
            return 1;
        case Feb:
            return 2;
        case Mar:
            return 3;
        case Apr:
            return 4;
        case May:
            return 5;
        case Jun:
            return 6;
        case Jul:
            return 7;
        case Aug:
            return 8;
        case Sep:
            return 9;
        case Nov:
            return 10;
        case Oct:
            return 11;
        case Dec:
            return 12;
    }
    return 0;
}

Date::Month Date::uintToMonth(unsigned value) {
    switch (value % 12) {
        case 1:
            return Jan;
        case 2:
            return Feb;
        case 3:
            return Mar;
        case 4:
            return Apr;
        case 5:
            return May;
        case 6:
            return Jun;
        case 7:
            return Jul;
        case 8:
            return Aug;
        case 9:
            return Sep;
        case 10:
            return Nov;
        case 11:
            return Oct;
        case 0:
            return Dec;
    }
    return Jan;
}

std::string Date::monthToString(Month value){
    switch (value) {
        case Jan:
            return "Jan";
        case Feb:
            return "Feb";
        case Mar:
            return "Mar";
        case Apr:
            return "Apr";
        case May:
            return "May";
        case Jun:
            return "Jun";
        case Jul:
            return "Jul";
        case Aug:
            return "Aug";
        case Sep:
            return "Sep";
        case Nov:
            return "Nov";
        case Oct:
            return "Oct";
        case Dec:
            return "Dec";
    }
    return "Jan";
}

std::string Date::toString() {
    std::string str = "";
    str += "Date: ";
    str += std::to_string(year) + "-";
    str += Date::monthToString(month) + "-";
    str += std::to_string(day) + " ";
    str += "Time: ";
    str += std::to_string(hour) + ":";
    str += std::to_string(minute) + ":";
    str += std::to_string(second);
    return str;
}

void Date::correctDate() {
    unsigned month_int = monthToUInt(month);
    minute += second / 60;
    second %= 60;
    hour += minute / 60;
    minute %= 60;
    day += hour / 24;
    hour %= 24;
    while (day / dayInMonth(month_int, year) != 0) {
        day -= dayInMonth(month_int, year);
        month_int++;
        year += month_int / 12;
        month_int %= 12;
    }
    month = uintToMonth(month_int);
    if (year > 9999) {
        year %= 9999;
    }
    year = year == 0 ? 1 : year;
}

Date Date::correctDateInterval(const DateInterval &interval) {
    unsigned new_year = this->year + std::abs(interval.getYear());
    unsigned new_month_i = monthToUInt(this->month) + std::abs(interval.getMonth());
    unsigned new_day = this->day + std::abs(interval.getDay());
    unsigned new_hour = this->hour + std::abs(interval.getHour());
    unsigned new_minute = this->minute + std::abs(interval.getMinute());
    unsigned new_second = this->second + std::abs(interval.getSecond());
    new_minute += new_second / 60;
    new_second %= 60;
    new_hour += new_minute / 60;
    new_minute %= 60;
    new_day += new_hour / 24;
    new_hour %= 24;
    while (new_day / dayInMonth(new_month_i, new_year) != 0) {
        new_day -= dayInMonth(new_month_i, new_year);
        new_month_i++;
        new_year += new_month_i / 12;
        new_month_i %= 12;
    }
    Month new_month = month = uintToMonth(new_month_i);
    if (new_year > 9999) {
        new_year -= 9999;
    }
    return Date(new_year, new_month, new_day, new_hour, new_minute, new_second);
}

bool Date::isLeapYear(unsigned year) {
    if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
        return true;
    }
    return false;
}

unsigned Date::dayInMonth(Date::Month month, unsigned year) {
    switch (month) {
        case Jan:
            return 31;
        case Feb:
            if (isLeapYear(year)) {
                return 29;
            }
            return 28;
        case Mar:
            return 31;
        case Apr:
            return 30;
        case May:
            return 31;
        case Jun:
            return 30;
        case Jul:
            return 31;
        case Aug:
            return 31;
        case Sep:
            return 30;
        case Nov:
            return 31;
        case Oct:
            return 30;
        case Dec:
            return 31;
    }
    return 0;
}

unsigned Date::dayInMonth(unsigned month, unsigned year) {
    switch (month % 12) {
        case 1:
            return 31;
        case 2:
            if (isLeapYear(year)) {
                return 29;
            }
            return 28;
        case 3:
            return 31;
        case 4:
            return 30;
        case 5:
            return 31;
        case 6:
            return 30;
        case 7:
            return 31;
        case 8:
            return 31;
        case 9:
            return 30;
        case 10:
            return 31;
        case 11:
            return 30;
        case 0:
            return 31;
    }
    return 0;
}

DateInterval Date::getInterval(const Date &date) {
    return DateInterval(*this, date);
}

Date Date::setInterval(const DateInterval &interval) {
    return correctDateInterval(interval);
}

std::string Date::formatDate(std::string format) {
    std::string result = format;
    size_t year_pos = result.find("YYYY");
    size_t day_pos = result.find("DD");
    size_t hour_pos = result.find("hh");
    size_t minute_pos = result.find("mm");
    size_t second_pos = result.find("ss");
    if ((year_pos == std::string::npos) ||
            (day_pos == std::string::npos) ||
            (hour_pos == std::string::npos) ||
            (minute_pos == std::string::npos) ||
            (second_pos == std::string::npos)) {
        return "Invalid date format";
    }
    result.replace(year_pos, std::string("YYYY").length(), std::to_string(year));
    result.replace(day_pos, std::string("DD").length(), std::to_string(day));
    result.replace(hour_pos, std::string("hh").length(), std::to_string(hour));
    result.replace(minute_pos, std::string("mm").length(), std::to_string(minute));
    result.replace(second_pos, std::string("ss").length(), std::to_string(second));

    size_t month_pos = result.find("MMM");
    if (month_pos == std::string::npos) {
        month_pos = result.find("MM");
        if (month_pos == std::string::npos) {
            return "Invalid date format";
        }
        result.replace(month_pos, std::string("MM").length(), std::to_string(monthToUInt(month)));
    } else {
        result.replace(month_pos, std::string("MMM").length(), monthToString(month));
    }
    return result;
}

Date Date::addYear(int value) {
    return Date(year + value, month, day, hour, minute, second);
}

Date Date::addMonth(int value) {
    return Date(year, uintToMonth(monthToUInt(month) + value), day, hour, minute, second);
}

Date Date::addDay(int value) {
    return Date(year, month, day + value, hour, minute, second);
}

Date Date::addHour(int value) {
    return Date(year, month, day, hour + value, minute, second);
}

Date Date::addMinute(int value) {
    return Date(year, month, day, hour, minute + value, second);
}

Date Date::addSecond(int value) {
    return Date(year, month, day, hour, minute, second + value);
}

unsigned Date::getYear() const {
    return year;
}

Date::Month Date::getMonth() const {
    return month;
}

unsigned Date::getDay() const {
    return day;
}

unsigned Date::getHour() const {
    return hour;
}

unsigned Date::getMinute() const {
    return minute;
}

unsigned Date::getSecond() const {
    return second;
}

void Date::setYear(unsigned value) {
    this->year = value;
    correctDate();
}

void Date::setMonth(Date::Month value) {
    this->month = value;
    correctDate();
}

void Date::setDay(unsigned value) {
    this->day = value;
    correctDate();
}

void Date::setHour(unsigned value) {
    this->hour = value;
    correctDate();
}

void Date::setMinute(unsigned value) {
    this->minute = value;
    correctDate();
}

void Date::setSecond(unsigned value) {
    this->second = value;
    correctDate();
}

bool Date::operator==(const Date &date) {
    if ((year == date.year)
        && (month == date.month)
        && (day == date.day)
        && (hour == date.hour)
        && (minute == date.minute)
        && (second == date.second)) {
        return true;
    }
    return false;
}

Date::~Date() {

}
