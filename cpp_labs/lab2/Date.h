//
// Created by vlad on 05.10.17.
//
#ifndef DATE
#define DATE

#include <iostream>
#include <cmath>
#include <time.h>
#include "DateInterval.h"
class DateInterval;

class Date {
public:
    enum Month {
        Jan = 1,
        Feb = 2,
        Mar = 3,
        Apr = 4,
        May = 5,
        Jun = 6,
        Jul = 7,
        Aug = 8,
        Sep = 9,
        Oct = 10,
        Nov = 11,
        Dec = 12
    };

    //Constructor without parameters with current date and time
    Date();

    //Constructor of copy
    Date(const Date &obj);

    //Constructor with full parameters of date and time
    Date(unsigned year, Month month, unsigned day, unsigned hour, unsigned minute, unsigned second);

    //Constructor with parameters of date and 00.00.00 time
    Date(unsigned year, Month month, unsigned day);

    //Constructor with parameters of time and current date
    Date(unsigned hour, unsigned minute, unsigned second);

    //Convert month into unsigned int
    static unsigned monthToUInt(Month value);

    //Convert unsigned int into Month
    static Month uintToMonth(unsigned value);

    //Convert date and time to string "YYYY-MMM-DD hh:mm:ss"
    std::string toString();

    //Convert Month to string
    static std::string monthToString(Month value);

    //Check if this leap-year
    static bool isLeapYear(unsigned year);

    //Return days in month
    static unsigned dayInMonth(Month month, unsigned year);

    //Return days in month
    static unsigned dayInMonth(unsigned month, unsigned year);

    //Create new correct date with inteval
    Date correctDateInterval(const DateInterval &interval);

    //Count interval between current date and date
    DateInterval getInterval(const Date &date);

    //Count new date with interval
    Date setInterval(const DateInterval &interval);

    std::string formatDate(std::string format);

    Date addYear(int value);

    Date addMonth(int value);

    Date addDay(int value);

    Date addHour(int value);

    Date addMinute(int value);

    Date addSecond(int value);

    unsigned getYear() const;

    Month getMonth() const;

    unsigned getDay() const;

    unsigned getHour() const;

    unsigned getMinute() const;

    unsigned getSecond() const;

    void setYear(unsigned value);

    void setMonth(Month value);

    void setDay(unsigned value);

    void setHour(unsigned value);

    void setMinute(unsigned value);

    void setSecond(unsigned value);

    bool operator==(const Date &date);

    //Destructor
    ~Date();

private:
    time_t time_d;
    tm *localTime_d;
    unsigned year;
    Month month;
    unsigned day;
    unsigned hour;
    unsigned minute;
    unsigned second;

    //Correct date and time
    void correctDate();
};

class DateInterval {
public:
    //Default constructor without parameters
    DateInterval();

    //Constructor with one date
    DateInterval(const Date &date);

    //Constructor with dwo dates
    DateInterval(const Date &first_date, const Date &second_date);

    //Constructor with full parameters of date and time
    DateInterval(int year, int month, int day, int hour, int minute, int second);

    //Constructor of copy
    DateInterval(const DateInterval &obj);

    //Count interval between first and second date
    void countInterval(const Date &first_date, const Date &second_date);

    void correctDate();

    //Convert date and time to string "YYYY-MMM-DD hh:mm:ss"
    std::string toString();

    int getYear() const;

    void setYear(int year);

    int getMonth() const;

    void setMonth(int month);

    int getDay() const;

    void setDay(int day);

    int getHour() const;

    void setHour(int hour);

    int getMinute() const;

    void setMinute(int minute);

    int getSecond() const;

    void setSecond(int second);

    bool operator==(const DateInterval &date);

    //Destructor
    ~DateInterval();

private:
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;
};

#endif //DATE
