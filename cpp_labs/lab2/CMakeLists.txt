cmake_minimum_required(VERSION 3.8)
project(lab2)

set(CMAKE_CXX_STANDARD 11)
add_subdirectory(lib/googletest)

include_directories(lib/googletest/googletest/include)
set(SOURCE_FILES main.cpp Date.h Date.cpp DateInterval.cpp DateInterval.h)
add_executable(lab2 ${SOURCE_FILES})
target_link_libraries(lab2 gtest)