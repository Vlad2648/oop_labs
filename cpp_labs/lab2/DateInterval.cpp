//
// Created by vlad on 16.10.17.
//


#include "Date.h"

DateInterval::DateInterval() {
    this->year = 0;
    this->month = 0;
    this->day = 0;
    this->hour = 0;
    this->minute = 0;
    this->second = 0;
}

DateInterval::DateInterval(const Date &date) {
    this->year = 0;
    this->month = 0;
    this->day = 0;
    this->hour = 0;
    this->minute = 0;
    this->second = 0;
}

DateInterval::DateInterval(const Date &first_date, const Date &second_date) {
    countInterval(first_date, second_date);
}

DateInterval::DateInterval(int year, int month, int day, int hour, int minute, int second) {
    this->year = year;
    this->month = month;
    this->day = day;
    this->hour = hour;
    this->minute = minute;
    this->second = second;
}

DateInterval::DateInterval(const DateInterval &obj) {
    this->year = obj.year;
    this->month = obj.month;
    this->day = obj.day;
    this->hour = obj.hour;
    this->minute = obj.minute;
    this->second = obj.second;
}

void DateInterval::countInterval(const Date &first_date, const Date &second_date) {
    this->year = first_date.getYear() - second_date.getYear();
    this->month = first_date.getMonth() - second_date.getMonth();
    this->day = first_date.getDay() - second_date.getDay();
    this->hour = first_date.getHour() - second_date.getHour();
    this->minute = first_date.getMinute() - second_date.getMinute();
    this->second = first_date.getSecond() - second_date.getSecond();
    correctDate();
}

void DateInterval::correctDate() {
    if (year > 0) {
        if(second < 0) {
            second = 60 + second;
            minute--;
        }
        if(minute < 0) {
            minute = 60 + minute;
            hour--;
        }
        if(hour < 0) {
            hour = 24 + hour;
            day--;
        }
        if(day < 0) {
            day = Date::dayInMonth(month, year) + day;
            month--;
        }
        if(month < 0) {
            month = 12 + month;
            year--;
        }
    }
}

std::string DateInterval::toString() {
    std::string str = "";
    str += "Date: ";
    str += std::to_string(year) + "-";
    str += std::to_string(month) + "-";
    str += std::to_string(day) + " ";
    str += "Time: ";
    str += std::to_string(hour) + ":";
    str += std::to_string(minute) + ":";
    str += std::to_string(second);
    return str;
}

int DateInterval::getYear() const {
    return year;
}

void DateInterval::setYear(int year) {
    this->year = year;
}

int DateInterval::getMonth() const {
    return month;
}

void DateInterval::setMonth(int month) {
    this->month = month;
}

int DateInterval::getDay() const {
    return day;
}

void DateInterval::setDay(int day) {
    this->day = day;
}

int DateInterval::getHour() const {
    return hour;
}

void DateInterval::setHour(int hour) {
    this->hour = hour;
}

int DateInterval::getMinute() const {
    return minute;
}

void DateInterval::setMinute(int minute) {
    this->minute = minute;
}

int DateInterval::getSecond() const {
    return second;
}

void DateInterval::setSecond(int second) {
    this->second = second;
}

bool DateInterval::operator==(const DateInterval &date) {
    if ((year == date.year)
        && (month == date.month)
        && (day == date.day)
        && (hour == date.hour)
        && (minute == date.minute)
        && (second == date.second)) {
        return true;
    }
    return false;
}

DateInterval::~DateInterval() {

}
