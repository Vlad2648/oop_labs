#include <iostream>
#include "Date.h"


//define for google test
#define GTEST

#ifdef GTEST
#include <gtest/gtest.h>

TEST(DataTest0, CorrectDate1) {
    Date date(1999, Date::Dec, 91, 26, 90, 90);
    ASSERT_TRUE(date == Date(2000, Date::Mar, 1, 3, 31, 30));
}
TEST(DataTest0, CorrectDate2) {
    Date date(2000, Date::Dec, 91, 26, 90, 90);
    ASSERT_TRUE(date == Date(2001, Date::Mar, 2, 3, 31, 30));
}
TEST(DataTest0, CorrectDate3) {
    Date date(2099, Date::Dec, 91, 26, 90, 90);
    ASSERT_TRUE(date == Date(2100, Date::Mar, 2, 3, 31, 30));
}
TEST(DataTest0, CorrectDate4) {
    Date date(0, Date::Jan, 0, 0, 0, 0);
    ASSERT_TRUE(date == Date(1, Date::Jan, 0, 0, 0, 0));
}
TEST(DataTest1, CopyDate1) {
    Date date;
    Date date_c(date);
    ASSERT_TRUE(date == date_c);
}
TEST(DataTest1, CopyDate2) {
    Date date(1941, Date::Sep, 1, 5, 30, 0);
    Date date_c(date);
    ASSERT_TRUE(date == date_c);
}
TEST(DataTest2, AddYear) {
    Date date(1941, Date::Sep, 1, 5, 30, 0);
    Date date_a = date.addYear(4);
    ASSERT_TRUE(date_a == Date(1945, Date::Sep, 1, 5, 30, 0));
}
TEST(DataTest2, AddMonth) {
    Date date(1941, Date::Sep, 1, 5, 30, 0);
    Date date_a = date.addMonth(3);
    ASSERT_TRUE(date_a == Date(1941, Date::Dec, 1, 5, 30, 0));
}
TEST(DataTest2, AddSubYear) {
    Date date(1941, Date::Sep, 1, 5, 30, 0);
    Date date_a = date.addYear(-4);
    ASSERT_TRUE(date_a == Date(1937, Date::Sep, 1, 5, 30, 0));
}
TEST(IntervalTest0, Interval0) {
    Date date1(1941, Date::Sep, 1, 5, 30, 0);
    Date date2(1945, Date::Sep, 1, 5, 30, 0);
    DateInterval interval(date2, date1);
    ASSERT_TRUE(interval == DateInterval(4, 0, 0, 0, 0, 0));
}
TEST(IntervalTest0, Interval1) {
    Date date1(1, Date::Jan, 1, 1, 1, 1);
    Date date2(2025, Date::Oct, 5, 8, 32, 20);
    DateInterval interval(date2, date1);
    ASSERT_TRUE(interval == DateInterval(2024, 9, 4, 7, 31, 19));
}
TEST(IntervalTest0, Interval2) {
    Date date1(1, Date::Jan, 1, 1, 1, 1);
    Date date2(2025, Date::Oct, 5, 8, 32, 20);
    DateInterval interval(date1, date2);
    ASSERT_TRUE(interval == DateInterval(-2024, -9, -4, -7, -31, -19));
}
TEST(IntervalTest0, Interval3) {
    Date date1(2000, Date::Dec, 30, 13, 50, 50);
    Date date2(2001, Date::Jan, 1, 1, 1, 1);
    DateInterval interval(date2, date1);
    ASSERT_TRUE(interval == DateInterval(0, 0, 1, 11, 10, 11));
}
TEST(IntervalTest0, Interval4) {
    Date date1(9999, Date::Jan, 30, 13, 50, 50);
    Date date2(2, Date::Jan, 30, 13, 50, 50);
    DateInterval interval(date2, date1);
    ASSERT_TRUE(interval == DateInterval(-9997, 0, 0, 0, 0, 0));
}
TEST(FormDate0, form0) {
    Date date(2000, Date::Dec, 30, 13, 50, 50);
    std::cout << date.formatDate("YYYY-MM-DD  hh:mm:ss") << std::endl;
    ASSERT_TRUE(date.formatDate("YYYY-MMM-DD  hh:mm:ss") == "2000-Dec-30  13:50:50");
}
TEST(FormDate0, form1) {
    Date date(2000, Date::Dec, 30, 13, 50, 50);
    ASSERT_TRUE(date.formatDate("YYYY-MM-DD  hh:mm:ss") == "2000-12-30  13:50:50");
}
int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
#else
int main(int argc, char* argv[]) {
    Date date;
    std::cout << date.toString() << std::endl;
    return 0;
}
#endif