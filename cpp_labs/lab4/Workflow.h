//
// Created by vlad on 14.12.17.
//

#ifndef LAB4_WORKFLOW_H
#define LAB4_WORKFLOW_H

#include "Parser.h"
#include "Workers.h"

#include <iostream>

class Workflow {
public:

    Workflow() = delete;

    Workflow(const std::string &wfFileName) : wfFileName(wfFileName),
                                           ioKey(false),
                                           ifName(""),
                                           ofName(""),
                                           parser(Parser(wfFileName)){}

    Workflow(const std::string &wfFileName,
             const std::string &ifName,
             const std::string &ofName) : wfFileName(wfFileName),
                                          ioKey(true),
                                          ifName(ifName),
                                          ofName(ofName),
                                          parser(Parser(wfFileName)){}

    //Execute all instructions from workflow file
    void execute();

private:

    const std::string wfFileName;

    Parser parser;

    const bool ioKey;

    const std::string ifName;

    const std::string ofName;

};

#endif //LAB4_WORKFLOW_H
