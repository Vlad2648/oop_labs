//
// Created by vlad on 14.12.17.
//

#include "Parser.h"

const std::string Parser::getWordByIndex(const std::string &str, const size_t index) throw(ParserExecuteException) {
    std::string result;
    size_t len = str.length();
    size_t start = 0;
    size_t end = 0;
    size_t wordNum = 1;

    while (true) {
        while (str[end] == ' ') {
            if (end == len - 1) {
                break;
            }
            ++start;
            ++end;
        }
        end = str.find(' ', end);
        if (end == std::string::npos) {
            if(wordNum == index) {
                result = str.substr(start);
                break;
            }
            throw ParserExecuteException("There is no word with index" + std::to_string(index) + " in line " + str);
        }
        if(wordNum == index) {
            result = str.substr(start, end - start);
            break;
        } else {
            ++wordNum;
            start = end;
        }
    }

    return result;
}

const std::string Parser::removeAllSpaces(const std::string &str) {
    std::string result = "";

    for(const auto& c : str) {
        if (c != ' ') {
            result += c;
        }
    }

    return result;
}

void Parser::parseFile() throw(ParserExecuteException, std::ifstream::failure) {
    std::ifstream file;
    std::string line;

    file.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try {
        file.open(fileName);

        //check for "desc"
        std::getline(file, line);
        if (removeAllSpaces(line) != "desc") {
            throw ParserExecuteException("Something wrong with input file (no \"desc\")");
        }

        //find all instructions line
        while(true) {
            if (file.eof()) {
                throw ParserExecuteException("Something wrong with input file (no \"csed\")");
            }
            std::getline(file, line);
            //check for csed
            if (removeAllSpaces(line) == "csed") {
                break;
            }
            parseAndSaveInstructLine(line);
            //count instructions
            ++count;
        }

        //parse and save order of instructions
        if (!file.eof()) {
            std::getline(file, line);
            parseAndSaveOrderLine(line);
        } else {
            throw ParserExecuteException("Something wrong with input file (no order line)");
        }

        file.close();

        //check for correct input
        if (instructions.size() != order.size()) {
            throw ParserExecuteException("Something wrong with input file (different number of instruction and order) in line: ");
        }

    } catch (std::ifstream::failure &e) {
        throw;
    }
}

const Worker *Parser::nextWorker() {
    return step < instructions.size() ? instructions.at(order[step++]) : nullptr;
}

const bool Parser::checkCorrect() {
    if ((instructions.size() == 0) || (order.size() == 0)) {
        return false;
    }
    const Worker *fist_worker = instructions[order[0]];
    const Worker *last_worker = instructions[order[order.size() - 1]];
    return (fist_worker->getName() == "readfile") || (last_worker->getName() == "writefile");
}

void Parser::parseAndSaveInstructLine(const std::string line) throw(ParserExecuteException) {
    int instrucNum = 0;

    //find instruction number and check it
    try {
        instrucNum = std::stoi(line);
    } catch (...) {
        throw ParserExecuteException("Something wrong with input file (incorrect number of instruction) in line: " + std::to_string(count));
    }
    if (instructions.count(instrucNum) != 0) {
        throw ParserExecuteException("Something wrong with input file (incorrect instruction line) in line: " + std::to_string(count));
    }

    //check for '='
    try {
        if (getWordByIndex(line, 2) != "=") {
            throw ParserExecuteException("Something wrong with input file (incorrect instruction line) in line: " + std::to_string(count));
        }
    } catch (...) {
        throw ParserExecuteException("Something wrong with input file (incorrect instruction line) in line: " + std::to_string(count));
    }

    //find instructions and save it
    try {
        std::string instruct = getWordByIndex(line, 3);
        if(instruct == "readfile") {
            std::vector<std::string> args;
            args.push_back(getWordByIndex(line, 4));

            instructions[instrucNum] = Factory::constructWorker(instruct, args);
        } else
        if(instruct == "writefile") {
            std::vector<std::string> args;
            args.push_back(getWordByIndex(line, 4));

            instructions[instrucNum] = Factory::constructWorker(instruct, args);
        } else
        if(instruct == "grep") {
            std::vector<std::string> args;
            args.push_back(getWordByIndex(line, 4));

            instructions[instrucNum] = Factory::constructWorker(instruct, args);
        } else
        if(instruct == "sort") {
            std::vector<std::string> args;

            instructions[instrucNum] = Factory::constructWorker(instruct, args);
        } else
        if(instruct == "replace") {
            std::vector<std::string> args;
            args.push_back(getWordByIndex(line, 4));
            args.push_back(getWordByIndex(line, 5));

            instructions[instrucNum] = Factory::constructWorker(instruct, args);
        } else
        if(instruct == "dump") {
            std::vector<std::string> args;
            args.push_back(getWordByIndex(line, 4));

            instructions[instrucNum] = Factory::constructWorker(instruct, args);
        } else {
            throw ParserExecuteException("Something wrong with input file (incorrect name of instruction) in line: " + std::to_string(count));
        }
    } catch (...) {
        throw ParserExecuteException("Something wrong with input file (incorrect instruction line) in line: " + std::to_string(count));
    }
    if (instructions[instrucNum] == nullptr) {
        throw ParserExecuteException("Something wrong with fabric in line:" + std::to_string(count));
    }

}

void Parser::parseAndSaveOrderLine(const std::string line) throw(ParserExecuteException){
    std::string str = removeAllSpaces(line) + ">";
    size_t index = 0;
    int value = 0;

    try {
        for (size_t i = 0; i < count; ++i) {
            value = std::stoi(str);
            order.push_back(value);
            index = str.find('>') + 1;
            str = str.substr(index);
        }
    } catch (...) {
        throw ParserExecuteException("Something wrong with input file (no order line)");
    }
}

Parser::~Parser() {
    for(const auto& worker : instructions) {
        delete worker.second;
    }
}
