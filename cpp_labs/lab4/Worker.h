//
// Created by vlad on 09.12.17.
//

#ifndef LAB4_WORKER_H
#define LAB4_WORKER_H

#include <string>
#include <vector>
#include <exception>

//Exception for Worker and extends classes
class WorkerExecuteException : public std::exception {
public:

    //Default constructor for WorkerExecuteException with reason "Nothing"
    WorkerExecuteException() : reason("Nothing") {}

    //Single-argument constructor with reason of exception
    explicit WorkerExecuteException(const std::string &description) : reason(description) {}

    //Return the reason of exception
    const char *what() const throw() override {
        return reason.c_str();
    }

private:

    const std::string reason;

};

//Container for Data of workflow project
class Result {
public:

    //Default constructor with empty data
    Result() : isEmpty(true) {}

    //Single-argument constructor with some data
    explicit Result(const std::vector<std::string> &value) : data(value) {
        isEmpty = (value.size() != 0) ? false : true;
    }

    Result(const Result &copy) : data(copy.data), isEmpty(copy.isEmpty) {}

    //Check if data is empty
    const bool isEmty() const {
        return isEmpty;
    }

    //Get data
    const std::vector<std::string> getData() const throw(WorkerExecuteException) {
        if (isEmpty) {
            throw WorkerExecuteException("Data is empty");
        }
        return data;
    }

    Result &operator=(const Result &other) {
        data = other.data;
        isEmpty = other.isEmpty;
        return *this;
    }

    const bool operator==(const Result &other) const {
        return (isEmpty == other.isEmpty) && (data == other.data);
    }

private:

    bool isEmpty;

    std::vector<std::string> data;

};

//Interface for workers
class Worker {
public:

    //Virtual method for all workers
    virtual const Result execute(const Result &previous) const throw(WorkerExecuteException) = 0;

    virtual const std::string getName() const = 0;

};

#endif //LAB4_WORKER_H
