//
// Created by vlad on 14.12.17.
//

#include "Workflow.h"

void Workflow::execute() {
    Result currentResult;
    Worker const* worker = nullptr;

    try {
        parser.parseFile();
        worker = parser.nextWorker();
    } catch (std::ifstream::failure &e) {
        std::cout << "Can't open workflow file" << std::endl;
        return;
    } catch (ParserExecuteException &e) {
        std::cout << e.what() << std::endl;
        return;
    } catch (std::out_of_range &e) {
        std::cout << "Out of range: " << e.what() << std::endl;
    } catch (...) {
        std::cout << "WHAT???" << std::endl;
        return;
    }

    if (worker == nullptr) {
        return;
    }

    if (!ioKey) {
        if (!parser.checkCorrect()) {
            std::cout << "Incorrect workflow file" << std::endl;
        }
        try {
            do {
                currentResult = worker->execute(currentResult);
                worker = parser.nextWorker();
            } while (worker != nullptr);
        } catch (WorkerExecuteException &e) {
            std::cout << e.what() << std::endl;
        } catch (std::out_of_range &e) {
            std::cout << "Out of range: "<< e.what() << std::endl;
        }
    } else {
        const Reader reader(ifName);
        const Writer writer(ofName);
        try {
            currentResult = reader.execute(currentResult);
            do {
                currentResult = worker->execute(currentResult);
                worker = parser.nextWorker();
            } while (worker != nullptr);
            writer.execute(currentResult);
        } catch (WorkerExecuteException &e) {
            std::cout << e.what() << std::endl;
        }
    }
}
