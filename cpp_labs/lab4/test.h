//
// Created by vlad on 16.12.17.
//

#ifndef LAB4_TEST_H
#define LAB4_TEST_H

#include "gtest/gtest.h"
#include "Workflow.h"

TEST(ParserTests, deleteSpace) {
    std::string str = "  1         2                                 3";
    std::string word = Parser::removeAllSpaces(str);
    ASSERT_TRUE(word == "123");
}

TEST(ParserTests, findWord_1) {
    std::string str = "     some  word   !!! ";
    std::string word = Parser::getWordByIndex(str, 1);
    ASSERT_TRUE(word == "some");
}

TEST(ParserTests, findWord_2) {
    std::string str = "     some  word   !!! ";
    std::string word = Parser::getWordByIndex(str, 2);
    ASSERT_TRUE(word == "word");
}

TEST(ParserTests, findWord_3) {
    std::string str = "     some  word   !!! ";
    std::string word = Parser::getWordByIndex(str, 3);
    ASSERT_TRUE(word == "!!!");
}

TEST(ParserTests, parseFile_1) {
    Parser parser;
    ASSERT_ANY_THROW(parser.parseFile());
}

TEST(ParserTests, parseFile_2) {
    std::string file = "tests/test_1.txt";
    Parser parser(file);
    try {
        parser.parseFile();
    } catch (...) {
        ASSERT_TRUE(false);
    }
    ASSERT_TRUE(true);
}

TEST(ParserTests, parseFile_3) {
    std::string file = "tests/test_2.txt";
    Parser parser(file);
    ASSERT_ANY_THROW(parser.parseFile());

}

TEST(ParserTests, parseFile_4) {
    std::string file = "tests/test_3.txt";
    Parser parser(file);
    ASSERT_ANY_THROW(parser.parseFile());
}

TEST(ParserTests, parseFile_5) {
    std::string file = "tests/test_4.txt";
    Parser parser(file);
    parser.parseFile();
    ASSERT_TRUE(parser.checkCorrect());
}

TEST(ResultTests, checkResult) {
    std::vector<std::string> data;
    Result res(data);
    ASSERT_TRUE(res.isEmty());
}

TEST(ResultTests, checkSort) {
    std::vector<std::string> data { "cab", "abc", "bca" };
    std::vector<std::string> check { "abc", "bca", "cab" };
    Result res(data);
    Sorter sort;
    res = sort.execute(res);
    ASSERT_TRUE(res.getData() == check);
}

TEST(ResultTests, checkGrep) {
    std::vector<std::string> data { "aaa bbb", "abc bbb", "abc aaa ccc", "aaa bbb aaa" };
    std::vector<std::string> check { "aaa bbb", "abc aaa ccc", "aaa bbb aaa" };
    Result res(data);
    Greper grep("aaa");
    res = grep.execute(res);
    ASSERT_TRUE(res.getData() == check);
}

TEST(ResultTests, checkReplace) {
    std::vector<std::string> data { "aaa abc", "abc aaa", "bca" };
    std::vector<std::string> check { "bbb abc", "abc bbb", "bca" };
    Result res(data);
    Replacer repl("aaa", "bbb");
    res = repl.execute(res);
    ASSERT_TRUE(res.getData() == check);
}

#endif //LAB4_TEST_H
