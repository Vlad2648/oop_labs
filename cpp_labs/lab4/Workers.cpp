//
// Created by vlad on 14.12.17.
//

#include "Workers.h"

const Worker *Factory::constructWorker(const std::string &name, const std::vector<std::string> &args) {
    if(name == "readfile") {
        if(args.size() == 1) {
            return new Reader(args[0]);
        }
    }
    if(name == "writefile") {
        if(args.size() == 1) {
            return new Writer(args[0]);
        }
    }
    if(name == "grep") {
        if(args.size() == 1) {
            return new Greper(args[0]);
        }
    }
    if(name == "sort") {
        if(args.size() == 0) {
            return new Sorter();
        }
    }
    if(name == "replace") {
        if(args.size() == 2) {
            return new Replacer(args[0], args[1]);
        }
    }
    if(name == "dump") {
        if(args.size() == 1) {
            return new Dumper(args[0]);
        }
    }
    return nullptr;
}
