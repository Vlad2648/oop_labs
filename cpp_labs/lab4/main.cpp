#include "Workflow.h"

#define GTEST

#ifdef GTEST
#include "test.h"

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#else

int main(int argc, char* argv[]) {
    if (argc == 2) {
        Workflow workflow(argv[1]);
        workflow.execute();
    } else
    if (argc == 6) {
        std::string ifName;
        std::string ofName;
        size_t indexFile = 1;
        for(size_t i = 1; i < argc - 1; ++i) {
            if (argv[i] == "-i") {
                ++i;
                ifName = argv[i];
            } else if (argv[i] == "-o") {
                ++i;
                ifName = argv[i];
            } else {
                indexFile = i;
            }
        }

        Workflow workflow(argv[indexFile], ifName, ofName);
        workflow.execute();
    } else {
        std::cout << "Write input workflow file" << std::endl;
    }
    return 0;
}

#endif //TEST
