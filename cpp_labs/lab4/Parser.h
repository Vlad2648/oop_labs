//
// Created by vlad on 14.12.17.
//

#ifndef LAB4_PARSER_H
#define LAB4_PARSER_H

#include <string>
#include <map>
#include <vector>
#include <fstream>
#include <exception>

#include "Workers.h"

//Exception for Parser
class ParserExecuteException : public std::exception {
public:

    //Default constructor for WorkerExecuteException with reason "Nothing"
    ParserExecuteException() : reason("Nothing") {}

    //Single-argument constructor with reason of exception
    explicit ParserExecuteException(const std::string &description) : reason(description) {}

    //Return the reason of exception
    const char *what() const throw() override {
        return reason.c_str();
    }

private:

    const std::string reason;

};

//Text parser for workflow project
class Parser {
public:

    Parser() = default;

    Parser(const std::string &fileName) : fileName(fileName), count(0), step(0) {}

    //Return word in str by index
    static const std::string getWordByIndex(const std::string &str, const size_t index) throw(ParserExecuteException);

    //Return new str without spaces
    static const std::string removeAllSpaces(const std::string &str);

    //Parse file and file instructions
    void parseFile() throw(ParserExecuteException, std::ifstream::failure);

    //Return next worker in order or nullptr if it end
    const Worker* nextWorker();

    //Check for input correct
    const bool checkCorrect();

    ~Parser();

private:

    std::map<int, const Worker*> instructions;

    std::vector<int> order;

    std::string fileName;

    size_t count;

    size_t step;

    //Parse instruction line and insert new instruction into map
    void parseAndSaveInstructLine(const std::string line) throw(ParserExecuteException);

    //Parse order line and add number into vector
    void parseAndSaveOrderLine(const std::string line) throw(ParserExecuteException);

};

#endif //LAB4_PARSER_H
