//
// Created by vlad on 09.12.17.
//

#ifndef LAB4_READER_H
#define LAB4_READER_H

#include "../Worker.h"
#include <fstream>

class Reader : public Worker {
public:

    Reader(const std::string &fileName) : fileName(fileName) {}

    //Read data from input file and return new Result
    const Result execute(const Result &previous) const throw(WorkerExecuteException) override;

    //Return class name
    const std::string getName() const override {
        return "readfile";
    }

private:

    const std::string fileName;

};

#endif //LAB4_READER_H
