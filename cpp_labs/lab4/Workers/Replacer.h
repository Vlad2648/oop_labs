//
// Created by vlad on 09.12.17.
//

#ifndef LAB4_REPLACER_H
#define LAB4_REPLACER_H

#include "../Worker.h"

class Replacer : public Worker {
public:

    Replacer(const std::string &currentWord, const std::string &newWord) : currentWord(currentWord), newWord(newWord) {}

    //Replace currentWord to newWord in str
    static const std::string replace(const std::string &str,
                                      const std::string &currentWord,
                                      const std::string &newWord);

    //Replace currentWord to newWord in all data and return new Result
    const Result execute(const Result &previous) const throw(WorkerExecuteException) override;

    //Return class name
    const std::string getName() const override {
        return "replace";
    }

private:

    const std::string currentWord;

    const std::string newWord;

};

#endif //LAB4_REPLACER_H
