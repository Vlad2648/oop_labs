//
// Created by vlad on 09.12.17.
//

#ifndef LAB4_DUMPER_H
#define LAB4_DUMPER_H

#include "../Worker.h"
#include <fstream>

class Dumper : public Worker {
public:

    Dumper(const std::string &fileName) : fileName(fileName) {}

    //Save all data into output file and return current Result
    const Result execute(const Result &previous) const throw(WorkerExecuteException) override;

    //Return class name
    const std::string getName() const override {
        return "dump";
    }

private:

    const std::string fileName;

};

#endif //LAB4_DUMPER_H
