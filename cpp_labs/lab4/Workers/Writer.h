//
// Created by vlad on 09.12.17.
//

#ifndef LAB4_WRITER_H
#define LAB4_WRITER_H

#include "../Worker.h"
#include <fstream>

class Writer : public Worker {
public:

    Writer(const std::string &fileName) : fileName(fileName) {}

    //Write all data in output file and return empty Result
    const Result execute(const Result &previous) const throw(WorkerExecuteException) override;

    //Return class name
    const std::string getName() const override {
        return "writefile";
    }

private:

    const std::string fileName;

};


#endif //LAB4_WRITER_H
