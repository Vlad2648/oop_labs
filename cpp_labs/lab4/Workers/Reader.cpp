//
// Created by vlad on 09.12.17.
//

#include "Reader.h"

const Result Reader::execute(const Result &previous) const throw(WorkerExecuteException){
    std::ifstream file;
    std::string line;
    std::vector<std::string> newData;

    file.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try {
        file.open(fileName);
        while(!file.eof() && std::getline(file, line)) {
            newData.push_back(line);
        }
        file.close();
    } catch (std::ifstream::failure& e) {
        if(!file.eof()) {
            throw WorkerExecuteException("Something wrong with input file \"" + fileName + "\"");
        }
    }

    return Result(newData);
}
