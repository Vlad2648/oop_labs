//
// Created by vlad on 09.12.17.
//

#include "Replacer.h"

const std::string Replacer::replace(const std::string &str,
                                    const std::string &currentWord,
                                    const std::string &newWord) {
    std::string result(str);
    size_t index = 0;

    while (true) {
        index = str.find(currentWord, index);
        if (index == std::string::npos) {
            break;
        }
        result.replace(index, currentWord.size(), newWord);
        index += newWord.size();
    }

    return result;
}

const Result Replacer::execute(const Result &previous) const throw(WorkerExecuteException) {
    std::vector<std::string> newDate;

    for(const auto& line : previous.getData()) {
        newDate.push_back(replace(line, currentWord, newWord));
    }

    return Result(newDate);
}
