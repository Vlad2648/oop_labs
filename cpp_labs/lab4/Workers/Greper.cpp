//
// Created by vlad on 09.12.17.
//

#include "Greper.h"

const Result Greper::execute(const Result &previous) const throw(WorkerExecuteException) {
    std::vector<std::string> newData;

    for(auto const& line : previous.getData()) {
        if(line.find(word) != -1) {
            newData.push_back(line);
        }
    }
    return Result(newData);
}
