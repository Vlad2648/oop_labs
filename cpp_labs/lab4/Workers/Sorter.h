//
// Created by vlad on 09.12.17.
//

#ifndef LAB4_SORTER_H
#define LAB4_SORTER_H

#include "../Worker.h"
#include <algorithm>

class Sorter : public Worker {
public:

    Sorter() = default;

    //Sort all line in data and return new Result
    const Result execute(const Result &previous) const throw(WorkerExecuteException) override;

    //Return class name
    const std::string getName() const override {
        return "sort";
    }

};

#endif //LAB4_SORTER_H
