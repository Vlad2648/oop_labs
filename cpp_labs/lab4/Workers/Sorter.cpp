//
// Created by vlad on 09.12.17.
//

#include "Sorter.h"

const Result Sorter::execute(const Result &previous) const throw(WorkerExecuteException) {
    std::vector<std::string> newData = previous.getData();
    std::sort(newData.begin(), newData.end());
    return Result(newData);
}
