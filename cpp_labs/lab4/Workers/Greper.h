//
// Created by vlad on 09.12.17.
//

#ifndef LAB4_GREPER_H
#define LAB4_GREPER_H

#include "../Worker.h"

class Greper : public Worker {
public:

    Greper(const std::string &word) : word(word) {}

    //Find all line with word and return new Result
    const Result execute(const Result &previous) const throw(WorkerExecuteException) override;

    //Return class name
    const std::string getName() const override {
        return "grep";
    }

private:

    const std::string word;

};

#endif //LAB4_GREPER_H
