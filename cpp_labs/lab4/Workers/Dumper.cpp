//
// Created by vlad on 09.12.17.
//

#include "Dumper.h"

const Result Dumper::execute(const Result &previous) const throw(WorkerExecuteException) {
    std::ofstream file;

    file.exceptions(std::ofstream::failbit | std::ofstream::badbit);

    try {
        file.open(fileName);
        for(const auto& line : previous.getData()) {
            file << line << std::endl;
        }
        file.close();
    } catch (std::ifstream::failure& e) {
        if(!file.eof()) {
            throw WorkerExecuteException("Something wrong with dump file \"" + fileName + "\"");
        }
    }

    return Result(previous.getData());
}
