//
// Created by vlad on 14.12.17.
//

#ifndef LAB4_WORKERS_H
#define LAB4_WORKERS_H

#include "Worker.h"
#include "Workers/Reader.h"
#include "Workers/Writer.h"
#include "Workers/Greper.h"
#include "Workers/Sorter.h"
#include "Workers/Replacer.h"
#include "Workers/Dumper.h"

//Factory for workers what return worker by his name or nullptr
class Factory {
public:

    //Return worker by his name or nullptr if it doesn't exist
    static const Worker* constructWorker(const std::string& name,
                                         const std::vector<std::string>& args);

};

#endif //LAB4_WORKERS_H
