#include "module1.h"
#include "module2.h"
#include "module3.h"
#include "files.h"
#include <iostream>
#include <fstream>
#include <list>

//#define SWITCH

int main(int argc, char* argv[]) {
#ifdef SWITCH
    using std::cout;
	cout << "Hello, world!!!" << std::endl;

	cout << Module1::getMyName() << std::endl;
	cout << Module2::getMyName() << std::endl;

	{
		using namespace Module1;
		cout << getMyName() << std::endl;
		cout << Module2::getMyName() << std::endl;
	}
	{
		using namespace Module2;
		cout << getMyName() << std::endl;
	}

	using Module2::getMyName;
	cout << getMyName() << std::endl;

	getchar();
#else
    using namespace std;
    Files temp(argv[1], argv[2]);
    if(temp.is_fail()) {
        cout << "Couldn't open file" << endl;
        return 0;
    }
    temp.read_from_file();
    temp.sort_string();
    temp.fprint_strings();
#endif
    return 0;
}