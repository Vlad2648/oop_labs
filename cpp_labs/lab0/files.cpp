#include "files.h"


Files::Files(char* input, char* output) {
    fin.open(input);
    fout.open(output);
}

bool Files::is_fail() {
    return fin.fail() || fout.fail();
}

void Files::read_from_file() {
    string temp;
    while (getline(fin, temp)){
        List.push_back(temp);
    }
}

void Files::sort_string() {
    List.sort();
}

void Files::print_strings() {
    for(string str : List) {
        cout << str << endl;
    }
}

void Files::fprint_strings() {
    for(string str : List) {
        fout << str << endl;
    }
}

Files::~Files() {
    fin.close();
    fout.close();
}

