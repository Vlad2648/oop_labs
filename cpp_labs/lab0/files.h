#include <fstream>
#include <string>
#include <list>
#include <iostream>

using namespace std;
class Files {
private:
    ifstream fin;
    ofstream fout;
    list<string> List;
public:
    Files(char* input, char* output);

    bool is_fail();

    void read_from_file();

    void sort_string();

    void print_strings();

    void fprint_strings();

    ~Files();

};